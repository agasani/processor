from pymongo import MongoClient
import boto.dynamodb2
from boto.dynamodb2.table import Table
import json
import sys
from decimal import Decimal
from pymongo import MongoClient
import mongoDataLayer
import time
from operator import itemgetter, attrgetter, methodcaller


import uuid

class Dyno:

  def __init__(self):

    self.dyno = boto.dynamodb2.connect_to_region('us-west-2',
                                           aws_access_key_id='AKIAIZJ2UP4H7M77ON2Q',
                                           aws_secret_access_key='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')
    #self.dyno_tables = {'feature_item': self.dyno.get_table('feature_item'), 'item':self.dyno.get_table('item')}#, 'category': self.dyno.get_table('category')}
    self.Items = Table('item_v3', connection=self.dyno)
    self.Features = Table('feature_item_v3', connection=self.dyno)
    #generate random new ids
    #new_id = hash(str(uuid.uuid1())) % 1000000000
    #hardcode synCategory for phone
    self.category_synonyms = ['cellphones', 'cell-phones', 'cellular', 'smartphone', 'telephone']
    self.increamentHack = 0.000000001

  def testDrive(self):
      #print 'This is just a test drive..'
      itemData = {'name': 'Test07', 'category': 'test', 'trend': 110, 'brand': 'testin', 'zip': 73130, 'isArticle': 1, 'time': '20150123', 'title': 'Reason why CriticJam is awesome',
              'desc': 'This is just testing saving and getting an item..', 'score': Decimal('0.8'), 'opNum': 128, 'upc': '7597594703',
                            'imgUrl': '[\"https://s3-us-west-2.amazonaws.com/imgurl/Apple++iPhone+4S+with+16GB+Memory+++.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/HTC++One+4G+LTE+.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/Motorola+DROID+4+4G+.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/Nokia++Lumia+925+.jpg\"]'}
      itemData1 = {'name': 'Test08', 'category': 'test3', 'trend': 30, 'brand': 'testa', 'zip': 73109, 'isArticle': 1, 'time': '20150115', 'title': 'The real reason the Amazon Fire Phone failed',
              'desc': 'This is just testing saving and getting an item..', 'score': Decimal('2'), 'opNum': 1280, 'upc': '7597594703',
                            'imgUrl': '[\"https://s3-us-west-2.amazonaws.com/imgurl/Apple++iPhone+4S+with+16GB+Memory+++.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/HTC++One+4G+LTE+.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/Motorola+DROID+4+4G+.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/Nokia++Lumia+925+.jpg\"]'}
      itemData2 = {'name': 'Test09', 'category': 'test', 'trend': 77, 'brand': 'testiez', 'zip': 73101, 'isArticle': 0, 'time': '20150117',
              'desc': 'This is just testing saving and getting an item..', 'score': Decimal('2.9'), 'opNum': 18, 'upc': '7597594703',
                            'imgUrl': '[\"https://s3-us-west-2.amazonaws.com/imgurl/Apple++iPhone+4S+with+16GB+Memory+++.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/HTC++One+4G+LTE+.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/Motorola+DROID+4+4G+.jpg\",\"https://s3-us-west-2.amazonaws.com/imgurl/Nokia++Lumia+925+.jpg\"]'}
      #items = [itemData, itemData1, itemData2]
      #print 'Batch adding items: ', self.addBatchItems(items)
      #print 'Adding an item: ', self.addItem(data)
      #item = self.getItem(itemData1['name'])
      #print 'The item is: ', item
      featData = {'itemName': 'Test09', 'score': Decimal('2.21'), 'stem': 'value', 'name': 'value', 'category': 'test',
                  'neg': '[\"purchase since there is no certainty that the product you buy is in good condition or function .\", \"they tried to make it everything and there are so many different ways to perform each function ( sending email , typing , calling , whatever ) that the phone gets confused\"]',
                  'pos': '[\"it takes the intuitive and functional nature of the blackberry , and the convenience of a touch screen , and joins\", \"bought a used one , arrived looking new and functioned just fine right out of the box ...\"]'}
      featData1 = {'itemName': 'Test09', 'score': Decimal('1.21'), 'stem': 'access', 'name': 'accessibility', 'category': 'test',
                  'neg': '[\"purchase since there is no certainty that the product you buy is in good condition or function .\", \"they tried to make it everything and there are so many different ways to perform each function ( sending email , typing , calling , whatever ) that the phone gets confused\"]',
                  'pos': '[\"it takes the intuitive and functional nature of the blackberry , and the convenience of a touch screen , and joins\", \"bought a used one , arrived looking new and functioned just fine right out of the box ...\"]'}
      featData2 = {'itemName': 'Test09', 'score': Decimal('2.40'), 'stem': 'weight', 'name': 'weight', 'category': 'test',
                  'neg': '[\"purchase since there is no certainty that the product you buy is in good condition or function .\", \"they tried to make it everything and there are so many different ways to perform each function ( sending email , typing , calling , whatever ) that the phone gets confused\"]',
                  'pos': '[\"it takes the intuitive and functional nature of the blackberry , and the convenience of a touch screen , and joins\", \"bought a used one , arrived looking new and functioned just fine right out of the box ...\"]'}
      featData3 = {'itemName': 'Test09', 'score': Decimal('1.17'), 'stem': 'wifi', 'name': 'wifi', 'category': 'test',
                  'neg': '[\"purchase since there is no certainty that the product you buy is in good condition or function .\", \"they tried to make it everything and there are so many different ways to perform each function ( sending email , typing , calling , whatever ) that the phone gets confused\"]',
                  'pos': '[\"it takes the intuitive and functional nature of the blackberry , and the convenience of a touch screen , and joins\", \"bought a used one , arrived looking new and functioned just fine right out of the box ...\"]'}
      #features = [featData, featData1, featData2, featData3]
      #print 'Batch adding features: ', self.addBatchFeatures(features)
      #print 'Adding a feature: ', self.addFeature(featData)
      #feat = self.getFeature(featData3['stem'], featData['category'])
      #print 'The feature is: ', feat

  def localMongoCollectionToDyno(self, category):
    local_collection = self.mongodb_local[category]
    local_items = local_collection.find()
    print 'About to migrate: ', local_items.count()
    print 'DynamoDB tables: ', self.dyno.list_tables()
    dyno_items = self.dyno.get_table('item')
    itemsidslist = []
    
    for item in local_items:
     try:
      range = item['Score']
      #eliminate features without reviews this should be done in the processor but for the time being..
      realFeatures = []
      for fe in item['Features']:
        if len(item['Features'][fe]['pos']) > 0 or len(item['Features'][fe]['neg']) > 0:
          realFeatures.append(fe)
      imgUrlPart = item['name'].replace(' ', '+') + '.jpg'
      #Remember to get descriptions from the phone data text file before updating dynamodb fileobj['specs']['description']
      #use name as primary hash instead of the id and shorten the name if need be
      name = item['name']
      if 'with' in name:
          name = name.split('with')[0]
      if ',' in name:
          name = name.split(',')[0]
      name = name.strip()
      key = name
      #itemsidslist.append(key)
      if len(item['description']) > 0:
          attrs = {'category': category, 'numTalks': item['NumReviews'], 'imgUrl': 'https://s3-us-west-2.amazonaws.com/imgurl/' + imgUrlPart, 'description': item['description'] }
      else:
          attrs = {'category': category, 'numTalks': item['NumReviews'], 'imgUrl': 'https://s3-us-west-2.amazonaws.com/imgurl/' + imgUrlPart}
      self.addItem(key, range, attrs)
      print 'Adding features..'
      for f in realFeatures:
        key = f
        range = item['Features'][f]['score']
        attrs = {'item_category': category, 'pos': json.dumps(item['Features'][f]['pos']), 'neg': json.dumps(item['Features'][f]['neg'])
        , 'name': item['Features'][f]['actualName'], 'item_name': name}
        self.addFeature(key, range, attrs)
     except:
       print 'Exception on:', item['name']
       print 'Exception: ', sys.exc_info()[0] 
    #self.addCategory(category, self.category_synonyms, itemsidslist)
   
  def addItem(self, data):
    try:
      self.Items.put_item(data=data)
      return True
    except Exception as err:
      raise
      print 'Some Error occured trying to put item: ', err.value
      return False

  def getItem(self, name):
    try:
        item = self.Items.query_2(name__eq=name, index='name-index', limit=1)
        return self.toObject(item.next())
    except Exception as err:
        raise
        print 'Some errors occured: ', err.value
        return {}

  def getFeature(self, stem, category):
    try:
        item = self.Features.query_2(stem__eq=stem, category__eq=category, index='stem-category-index', limit=1)
        return self.toObject(item.next())
    except Exception as err:
        print 'Some errors occured: ', err.value
        return {}

  def getFeatures(self, itemName, limit=7):
    try:
      features = self.Features.query_2(itemName=itemName, limit=limit)
      return features
    except Exception as err:
      print 'Some errors occured: ', err.value
      return {}

  def queryItems(self, category, **kwargs):

    try:
      limit = 7   # Default
      for k, v in kwargs.iteritems():
        if k == 'limit':
          limit = v
      itemResult = self.Items.query_2(category__eq=category, index='category-score-index', limit=limit, reverse=True)

      return list(itemResult)

    except Exception as err:
      #raise
      print 'Some errors occured trying to query item: ', err
      return []

  def addBatchItems(self, data):

    try:
      with self.Items.batch_write() as batch:
        for d in data:
          batch.put_item(data=d)
      return True

    except Exception as err:
        raise
        print 'Errors occured trying to batch_write: ', err.value
        return False
    
  def addFeature(self, data):
    try:
      self.Features.put_item(data=data)
      return True
    except Exception as err:
      raise
      print 'Some Error occured trying to put item: ', err.value
      return False
    
  def addBatchFeatures(self, data):
    try:
      with self.Features.batch_write() as batch:
        for d in data:
          batch.put_item(data=d)
      return True
    except Exception as err:
      print 'Errors occured trying to batch_write: ', err.value
      return False
  
  def addCategory(self, category, listSyns, itemsidslist):
    pass
    
  def addBatchCategories(self):
    pass 
    
  def addBrand(self, brand, listSyns, itemsidslist):
    pass
    
  def addBatchBrands(self):
    pass
    
  def createSchemas(self):
    pass
    
  def dropSchemas(self):
    pass

  def getItemDetails(self, name):
        name = name.strip()
        itemQuery = self.dyno_tables['item'].query(name__eq=name, limit=1)
        item = itemQuery.next()
        featureResults = self.dyno.query('feature_item', index_name = 'item_name-score-index', limit=20, select='ALL_PROJECTED_ATTRIBUTES', scan_index_forward=False,
                                    key_conditions={ 'item_name': {"ComparisonOperator": "EQ", "AttributeValueList": [{"S": name}] }})
        features = []
        for f in featureResults['Items']:
            feat = {}
            if 'name' in f:
                feat['name'] = f['name'].values()[0].encode('utf-8')
            if 'score' in f:
                feat['score'] = float(f['score'].values()[0])
            if 'neg' in f:
                feat['neg'] = []
                neg = json.loads(f['neg'].values()[0])
                for s in neg:
                    feat['neg'].append(s.encode('utf-8'))
            if 'pos' in f:
                feat['pos'] = []
                pos = json.loads(f['pos'].values()[0])
                for s in pos:
                    feat['pos'].append(s.encode('utf-8'))
            features.append(feat)
        daItem = {}
        if 'name' in item:
                daItem['name'] = item['name'].encode('utf-8')
        if 'score' in item:
                daItem['score'] = float(item['score'])
        if 'imgUrl' in item:
                daItem['imgUrl'] = item['imgUrl'].encode('utf-8')
        daItem['features'] = features
        return daItem

  def _dynoResponseToJson(self, resp):
        items = []
        for p in resp:
            item = {}
            if 'name' in p:
                item['name'] = p['name'].values()[0]
            if 'id' in p:
                item['id'] = p['id'].values()[0]
            if 'score' in p:
                item['score'] = p['score'].values()[0]
            if 'features' in p:
                if isinstance(p['features'], list):
                    item['features'] = p['features']
                else:
                    item['features'] = p['features'].values()[0]
            if 'description' in p:
                item['description'] = p['description'].values()[0]
            if 'id' in p:
                item['id'] = p['id'].values()[0]
            if 'category' in p:
                item['category'] = p['category'].values()[0]
            if 'item_name' in p:
                item['item_name'] = p['item_name'].values()[0]
            if 'item_category' in p:
                item['item_category'] = p['item_category'].values()[0]
            if 'pos' in p:
                item['pos'] = p['pos'].values()[0]
            if 'neg' in p:
                item['neg'] = p['neg'].values()[0]
            if 'supercategory' in p:
                item['supercategory'] = p['supercategory'].values()[0]
            if 'imgUrl' in p:
                item['imgUrl'] = p['imgUrl'].values()[0]
            if 'videoUrl' in p:
                item['videoUrl'] = p['videoUrl'].values()[0]
            if 'numTalks' in p:
                item['numTalks'] = p['numTalks'].values()[0]

            items.append(item)

        return items

  def toObject(self, botoItem):

    obj = {}
    keys = botoItem.keys()
    for k in keys:
      temp = botoItem[k]
      if isinstance(temp, Decimal):
        obj[k] = float(temp)
      else:
        obj[k] = temp

    return obj

  def insertItem(self, products, subCategory=None):
    #the following is to avoid conditional conflict in dynamoDb #commented out coz I do this in the exception below..
    '''for i in range(0, len(products)):
        sco = products[i]['score']
        for j in range(i, len(products)):
            if products[j]['score'] == sco:
                if products[j]['score'] < 0:
                    products[j]['score'] -= 0.01
                else:
                    products[j]['score'] += 0.01'''
    for product in products:
        try:
            item = {}
            item['name'] = product['name'].strip(' ').lower()
            item['category'] = category.strip(' ').lower()
            if 'subCategory' in product:
                item['subCategory'] = product['subCategory'].replace(' ', '-').lower()
            else:
                item['subCategory'] = subCategory
            item['brand'] = product['brand'].replace(' ', '-').lower()
            item['score'] = Decimal(str(product['score']))
            item['isArticle'] = False
            item['opNum'] = len(product['reviews'])
            item['upc'] = product['UPC']
            item['ean'] = product['EAN']
            item['asin'] = product['ASIN']
            item['parentASIN'] = product['parentASIN']
            item['sources'] = {'amazon': len(product['reviews'])}
            item['img'] = product['images']
            item['time'] = str(time.time())
            item['size'] = product['size']
            print 'listPrice: ', product['listPrice']
            if product['listPrice'] is not None:
                item['price'] = Decimal(str(product['listPrice']))
            else:
                item['price'] = Decimal('1000000')
            item['detailsLink'] = product['detailsLink']
            item['affil'] = [{'link': product['offersLink'], 'price': int(product['usedPrice']), 'name': 'Amazon'}]
            sortedFeatures = sorted(product['features'], key=lambda p:p['score'])
            sortedFeatures = [f for f in sortedFeatures if f['score'] > 0 or f['score'] < 0]
            item['top2Pos'] = []
            item['top2Neg'] = []
            nameLink = '<a href="' + item['detailsLink'] + '"' + ' target="_blank"' + '>' + product['name'].capitalize() + '</a>' + \
                                   ' has a CriticJam score of ' + str(round(product['score'], 1)) + ' based on ' + str(item['opNum']) +\
                               ' reviews, opinions.'
            item['desc'] = product['desc'] + ' ' + nameLink
            top2Neg = []
            top2Pos = []
            print 'BEFORE doing features ##############'
            print 'SORTEDFEATURES ARE: ', len(sortedFeatures)
            for f in sortedFeatures:
                if f['stem'] not in featuresToIgnore and f['score'] < 0:
                    top2Neg.append(f)
                if len(top2Neg) == 2:
                    break
            #print 'top2NEG are: ', top2Neg
            sortedFeatures.reverse()
            for f in sortedFeatures:
                if f['stem'] not in featuresToIgnore and f['score'] > 0:
                    top2Pos.append(f)
                if len(top2Pos) == 2:
                    break
            #print 'top2Pos are: ', top2Pos
            sortedFeatures.reverse()
            for tp in top2Pos:
                print 'POS ', tp['name']
                item['top2Pos'].append({'feature': tp['name'], 'score': Decimal(str(tp['score']))})
            for tn in top2Neg:
                print 'NEG: ', tn['name']
                item['top2Neg'].append({'feature': tn['name'], 'score': Decimal(str(tn['score']))})
            '''
            if len(item['top2Pos']) > 0:
                item['desc'] += ' Best features are '
                for index, f in enumerate(item['top2Pos'], start=1):
                    if index == 1:
                        item['desc'] += ' ' + f['feature']
                    elif index == 2:
                        item['desc'] += ' and ' + f['feature'] + '.'
            if len(item['top2Neg']) > 0:
                item['desc'] += ' Worst features are '
                for index, f in enumerate(item['top2Neg'], start=1):
                    if index == 1:
                        item['desc'] += ' ' + f['feature']
                    elif index == 2:
                        item['desc'] += ' and ' + f['feature'] + '.'
            '''
            print 'DESC: ', item['desc']
            print 'AFTER SETTING DESCRIPTION...'
            dyno.addItem(item)
            mongo.updateSaved(product['name'])
            print 'SAVED ITEM: ', item['name']
            self.insertFeatures(sortedFeatures, item['name'], item['category'])

        except Exception as ex:
            print 'FAILED to save ITEM: ', item['name'], ' ITEM table has a hash and range key of category score, ' \
                            'meaning that things in same category with same score could override each other.'
            print 'EXCEPTION : ', ex
            #this the hack to get product to be saved in dynamoDB
            for i in range(0, 999):
                try:
                    if 4.9 > item['score'] > -4.9:
                        self.increamentHack += 0.000000001
                        if item['score'] < 0:
                            print 'the old score is: ', item['score']
                            item['score'] = Decimal(str(float(item['score']) - self.increamentHack))
                            print 'and the new score is: ', product['score']
                        elif item['score'] > 0:
                            print 'the old score is: ', item['score']
                            item['score'] = Decimal(str(float(item['score']) + self.increamentHack))
                            print 'the new score is: ', item['score']

                        inserted = self.tryInsertItemAgain(item, sortedFeatures)
                        if inserted:
                            break
                    else:
                        break
                except Exception as ex2:
                    print 'AGAIN! AGAIN, FAILED to save ITEM: ', item['name'], ' ITEM table has a hash and range key of category', \
                        item['category'],  '  and score of: ', item['score'], \
                            ' meaning that in the DB there is another item(s) with same keys.'
                    print 'EXCEPTION : ', ex2

  def tryInsertItemAgain(self, item, sortedFeatures):
      try:
        dyno.addItem(item)
        mongo.updateSaved(item['name'])
        print 'SAVED ITEM: ', item['name']
        self.insertFeatures(sortedFeatures, item['name'], item['category'])
        return True
      except Exception as ex:
          print 'AGAIN! AGAIN, FAILED to save ITEM: ', item['name'], ' ITEM table has a hash and range key of category', \
                        item['category'],  '  and score of: ', item['score'], \
                            ' meaning that in the DB there is another item(s) with same keys.'
          print 'EXCEPTION : ', ex
          return False

  def insertFeatures(self, sortedFeatures, itemName, itemCategory):
        #TODO WTF am I doing below?
        topFeatures = sortedFeatures[0:9]
        length = len(sortedFeatures)
        if length <= 18:
            remainingFeatures = sortedFeatures[9:length]
        else:
            remainingFeatures = sortedFeatures[(length-9):length]
        topFeatures.extend(remainingFeatures)
        #the following is to avoid conditional conflict in dynamoDb
        for i in range(0, len(topFeatures)):
            sco = topFeatures[i]['score']
            for j in range(i, len(topFeatures)):
                if topFeatures[j]['score'] == sco:
                    if topFeatures[j]['score'] < 0:
                        topFeatures[j]['score'] -= 0.01
                    elif topFeatures[j]['score'] > 0:
                        topFeatures[j]['score'] += 0.01
        for feat in topFeatures:
            try:
                if feat['score'] > 0 or feat['score'] < 0:
                    feature = {}
                    feature['category'] = itemCategory
                    feature['itemName'] = itemName
                    feature['name'] = feat['name']
                    if len(feat['neg']) > 0 > feat['score']:
                        feature['neg'] = feat['neg'][0:21]
                    if len(feat['pos']) > 0 < feat['score']:
                        feature['pos'] = feat['pos'][0:21]
                    feature['score'] = Decimal(str(feat['score']))
                    feature['stem'] = feat['stem']
                    dyno.addFeature(feature)
                    print 'saved feature: ', feature['name']
            except Exception as ex:
                print 'ITEMNAME: ', feature['itemName']
                print 'SCORE: ', feature['score']
                print 'CATEGORY: ', feature['category']
                print 'FAILED to save feature: ', feat['name'], ' for PRODUCT: ', itemName, ' FEATURE table ' \
                            ' in has a hash and range key of itemName and score meaning that thing with same itemName and score could overwrite'
                print 'EXCEPTION : ', ex
                #hack to have both features saved
                for i in range(1, 99):
                    try:
                        if 2.8 > feature['score'] > -2.8:
                            self.increamentHack += 0.000000001
                            if feature['score'] < 0:
                                feature['score'] = Decimal(str(float(feature['score']) - self.increamentHack))
                            else:
                                feature['score'] = Decimal(str(float(feature['score']) + self.increamentHack))
                            #topFeatures.append(feature)
                            inserted = dyno.addFeature(feature)
                            if inserted:
                                break
                        else:
                            break
                    except Exception as ex:
                        print 'In Feature for-loop exception and feature, i, score are: [', feature['name'], ' ', i, ' ', feature['score'], ']'

if __name__ == '__main__':
    dyno = Dyno()
    #dyno.testDrive()
    client = MongoClient()
    db = client.criticjam
    featuresToIgnore = ['issu', 'issue', 'problem', 'love', 'time', 'fine']
    collection = 'hoverboards'.strip(' ')
    category = 'hoverboards'.strip(' ')
    subCategory = 'smartboards'.strip(' ')
    mongo = mongoDataLayer.MongoDataLayer(collection)
    productCursor = mongo.getProductsNotInDynamo()
    #turn the cursor into an array
    products = []
    for p in productCursor:
        products.append(p)
    print 'ABOUT TO INSERT ', len(products), ' IN DYNODB'
    dyno.insertItem(products, subCategory)