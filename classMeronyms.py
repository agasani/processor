from pattern.en import parsetree
from pattern.search import search, taxonomy, Pattern
from xgoogle.search import GoogleSearch, SearchError
from collections import Counter
from nltk.corpus import swadesh
from pymongo import MongoClient
import nltk
import time
from random import randint

porter = nltk.PorterStemmer()

class Meronyms:

    def __init__(self, className):
        self.className = className
        self.Patterns = []
        self.PatternsForGoogleSearch = []
        self.ExcludeList = swadesh.words('en')
        self.ExcludeList.extend(['you', 'man', 'fly', 'anything', 'everything', 'something', 'someone', 'more', 'your', '&', 'any', 'the', 'news', 'a', '|', '-', '_', 'thing'])
        taxonomy.append(className, type='category')

        self.Patterns.append(Pattern.fromstring('NP of DT? PRP$? NUM? JJ? ADJP? CATEGORY'))
        self.PatternsForGoogleSearch.append('" of * ' + className + '"')

        self.Patterns.append(Pattern.fromstring('NP on DT? PRP$? NUM? JJ? ADJP? CATEGORY'))
        self.PatternsForGoogleSearch.append('"on * ' + className + '"')

        self.Patterns.append(Pattern.fromstring('CATEGORY with DT? PRP$? NUM? JJ? ADJP? NP'))
        self.PatternsForGoogleSearch.append('"' + className + ' with "')

        self.Patterns.append(Pattern.fromstring('CATEGORY without DT? PRP$? NUM? JJ? ADJP? NP'))
        self.PatternsForGoogleSearch.append('"' + className + ' without "')

        self.Patterns.append(Pattern.fromstring('CATEGORY \'s DT? PRP$? NUM? JJ? ADJP? NP'))
        self.PatternsForGoogleSearch.append(className + '\'s')

        self.Patterns.append(Pattern.fromstring('NP in DT? PRP$? NUM? JJ? ADJP? CATEGORY'))
        self.PatternsForGoogleSearch.append('" in * ' + className + '"')

        self.Patterns.append(Pattern.fromstring('CATEGORY NP'))
        self.PatternsForGoogleSearch.append(className)

        self.Patterns.append(Pattern.fromstring('NP CATEGORY'))
        self.PatternsForGoogleSearch.append(className)

        self.Patterns.append(Pattern.fromstring('CATEGORY whose NP'))
        self.PatternsForGoogleSearch.append('"' + className + ' whose "')

        self.Patterns.append(Pattern.fromstring('CATEGORY have DT? PRP$? NUM? JJ? ADJP? NP'))
        self.PatternsForGoogleSearch.append('"' + className + ' has OR have "')

        '''server = 'ds047198.mongolab.com'
        port = 47198
        self.mongolab_client = MongoClient(server, port)
        self.mongolab_db = self.mongolab_client['reviewize']'''
        self.client = MongoClient()
        self.collection = self.client.criticjam['meronyms']

        #MongoDb sample query
        #items = self.mongolab_db['Meronyms'].find({'className': className})

    def GetMeronyms(self):
        #TODO move the meronyms into dynamoDB and save the new ones there..
        #TODO uncomment the below and use the appropriete meronyms depending on the category..
        #self.mongolab_db.authenticate('criticJam', 'criticJam103')
        #meronyms = self.mongolab_db['Meronyms'].find({'className': self.className})

        meronyms = self.getExistingMeronyms()
        if len(meronyms) > 0:
            return meronyms

        meronymsByPattern = []
        print '..in getMeronyms.. self.PatternsGoogleSearch len: ', len(self.PatternsForGoogleSearch)
        for p in range(0, len(self.PatternsForGoogleSearch)):
            corporata = []
            print 'in the for loop..'
            try:
                print '..in the try block and the str to search for is: ', self.PatternsForGoogleSearch[p]
                gs = GoogleSearch(self.PatternsForGoogleSearch[p])
                gs.results_per_page = 100
                results = []
                print 'After gs def..'
                for i in range(0, 40):
                    delay = 1.1
                    rand = randint(1,5)
                    if rand == 5:
                        delay = 2.1
                    elif rand == 2:
                        delay = 1.0
                    elif rand == 3:
                        delay = 1.7
                    elif rand == 4:
                        delay = 1.5
                    else:
                        delay = 1.3
                    tmp = gs.get_results(delay=delay)
                    print '...in the inner for loop..'
                    if not tmp:
                        break
                    results.extend(tmp)
                    print 'Results length so far: ', len(results)
                for rs in results:
                    desc = rs.title.encode('utf8')
                    corporata.append(desc.lower())
                    print 'to corpora adding: ', rs.title.encode('utf8')
            except SearchError, e:
                for rs in results:
                    desc = rs.title.encode('utf8')
                    corporata.append(desc.lower())
                    print 'to corpora adding: ', rs.title.encode('utf8')
                print 'A search Error happened: %s' % e
                print 'Corporata len so far: ', len(corporata)
            nps = []
            for cor in corporata:
                parset = parsetree(cor)
                if self.Patterns[p].search(parset):
                    print 'Pattern matched for: ', cor
                    match =  self.Patterns[p].match(parset)
                    for w in match.words:
                        if  str(match.constraint(w))== 'Constraint(chunks=[\'NP\'])':
                            nps.append(w.string)
                else:
                    print 'Pattern not matched for: ', cor
            meronymsByPattern.extend(list(set(nps)))
        counta = Counter(meronymsByPattern)
        selected = []
        for m in counta:
            if counta[m] >= 3 and m not in self.ExcludeList:
                selected.append(m)
        print 'Meronyms appearing more than 3 times: ', selected
        #TODO save stems instead of a fullwords "what about the multiple words features? prolly save stem of each individual words
        # use the porter stemmer on the top
        stemmedMeronyms = []
        for w in selected:
            if ' ' not in w:
                stemmedMeronyms.append(porter.stem(w))
            else:
                ws = w.split(' ')
                f = ''
                for w in ws:
                    f = f + porter.stem(w) + ' '
                stemmedMeronyms.append(f.strip())
        print 'stemmed meronyms: ', stemmedMeronyms
        #TODO uncomment the below after fixed the connection issue
        #ms = {'className': self.className, 'meronyms': stemmedMeronyms}
        #self.mongolab_db.authenticate('reviewizer', 'reviewizer103')
        #self.mongolab_db['Meronyms'].insert(ms)
        self.insertMeronyms(stemmedMeronyms)
        return stemmedMeronyms

    def insertMeronyms(self, meronyms):
        try:
            self.collection.update({'name': self.className}, {'$set': {'meronyms': meronyms, 'name': self.className}}, True)
            print 'Successfully upserted meronyms..'
        except Exception as ex:
            print 'Failed to insert meronyms for: ', self.className
            raise

    def getExistingMeronyms(self):
        meronyms = self.collection.find({'name': self.className})
        if meronyms.count() > 0:
            return meronyms[0]['meronyms']
        else:
            return []

if __name__ == '__main__':
    print 'Here comes meronyms..'
    mero = Meronyms('phone')
    meronyms = mero.getExistingMeronyms()
    print 'Found ', len(meronyms), ' meronyms in the collection..'
