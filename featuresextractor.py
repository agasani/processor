import nltk
from decimal import *
import re
from pattern.web import URL
from pmiPruner import PMIPruner
from classMeronyms import Meronyms

porter = nltk.PorterStemmer()


#TODO in addition to stemming, Implement Fuzzy matching to make sure I get all occurency of a feature

getcontext().prec = 3
MinSup = Decimal(.01)
SubItemsets = []
Sentences = []
StemsDict = {}
PruneCompactSupport = 2
PruneWordDistance = 4
PruneRedundancyPSupport = 3
OneWordFeaturesPSupport = 2


Stuff = {'name': 'Samsung Galaxy S4', 'category': ['Phones', 'Cellphones'], 'model': 'S4', 'superCategory': 'Electronics', 'id': 1}

#this exclude list should remain empty the excluding happens in the 'pmiPruner'
excludeList = []
#excludeList.extend(Stuff['category'])
#excludeList.extend(Stuff['name'].split(' '))
#excludeList.extend(Stuff['model'].split(' '))
excludeList = list(set(excludeList))
excludeList = [porter.stem(e.lower()) for e in excludeList]

'''
Stuff['reviews'] = ['Its camera is the shit device in a camera. I really love the camera.', 'The images quality is good.', 'it\'s huge it cannot fit in a coach bag is really the straw and I love going screen.',
    'image qualities is great.', 'image is great and quality is fine.', 'the size is big and I love going screen.',
    'the screen size is ridiculous.', 'the screen is entertaining but comes at cost in size.', 'I love this phone. And I like the panorama game apps screen',
    'its cameras takes awesome pictures and viewing them on the big size screens is good', 'game apps make it an entertainment center with image quality',
    'the gesture control is sleek.', 'I got this phone as a gift and I love the gesture-control and I love going screen.']
'''

'''
fileName = 'Resources/sumsangG4Reviews.txt'
try:
  with open(fileName, 'r') as f:
    rawReviews = f.read()
    Stuff['reviews'] = rawReviews.split('<FIN>')
except:
  print 'Could not open ', fileName
  Stuff['reviews'] = []
'''

def Apriori_Gen(L):
    
    c = []
    
    for itemset in L:
        is_index = L.index(itemset)
        for nxt_index in range(is_index+1, len(L)):
            for f in L[nxt_index]:
                if f not in L[is_index]:
                  new_itemset = list(L[is_index])
                  new_itemset.append(f)
                  subSetitem = False
                  if len(new_itemset) > 2:
                      if set([new_itemset[0], new_itemset[1]]) in SubItemsets:
                        subSetitem = True
                      if set([new_itemset[0], new_itemset[2]]) in SubItemsets:
                        subSetitem = True
                      if set([new_itemset[1], new_itemset[2]]) in SubItemsets:
                        subSetitem = True
                      
                  if set(new_itemset) not in c and not subSetitem:
                    c.append(set(new_itemset))
    ct = []
    for it in c:
      ct.append(list(it))
    return ct

def subSupEliminator(C, featuresSets, MinSup):
    l = []
    for s in C:
        count = 0
        #print 'L is: ', s
        for seti in featuresSets:
          s = set(s)
          if s.issubset(seti):
            count += 1
            
        #else:
            #print 'This not accounted for: ', seti
        if Decimal(count)/Decimal(len(featuresSets)) >= MinSup:
          l.append(s)
        else:
          SubItemsets.append(s)
                  

    return l


# TODO re-work the regex part to have a consistant ting
# TODO make sure the punctuations marks are removed..
def Pre_Processor(listOfFeatures):
  
  processedFeatures = []
    
  for feat in listOfFeatures:
    originalFeat = feat.lower()
    feat = feat.lower()
    parts = re.findall(r'^([a-zA-Z0-9]*?)([^a-zA-Z0-9]+)([a-zA-Z0-9]*)$', feat)
    if len(parts) > 0:
      processedPart = ''
      lparts = list(parts[0])
      for part in lparts:
          
          if re.search('[a-zA-Z]+', part):
            processedPart += part
      feat = processedPart
    stem = porter.stem(feat)
    if stem not in excludeList:
      processedFeatures.append(stem)
      StemsDict[originalFeat] = stem

  return processedFeatures


def GetOriginalOrStem(stem):
  
  originals = [key for key in StemsDict.keys() if StemsDict[key] == stem]
  toUse = [orig for orig in originals if len(orig) <= len(stem)]
    
  if len(toUse) > 0:
    if stem[len(stem)-1] != toUse[0][len(toUse[0])-1]:
      return stem[0: len(stem)-1]
    else:     
      return stem
  else:    
    return stem


def PrunePF(features, sentences):

  pruned = []
    
  if(len(features) > 0):
    if len(features[0]) == 2:    
      for f in features:
        compactness = 0
        feat1 = GetOriginalOrStem(list(f)[0])
        feat2 = GetOriginalOrStem(list(f)[1])
        for sent in [s for s in sentences if s.find(feat1) != -1 and s.find(feat2) != -1]:
          i1 = sent.find(feat1) if sent.find(feat1) < sent.find(feat2) else sent.find(feat2)
          i2 = sent.find(feat1) if sent.find(feat1) > sent.find(feat2) else sent.find(feat2)
          if sent[i1:i2].count(' ') <= PruneWordDistance:
            compactness += 1
          if compactness == PruneCompactSupport:
            break
        if compactness >= PruneCompactSupport:
          pruned.append(f)          
      
    elif len(features[0]) == 3:
      for f in features:
        compactness = 0
        feat1 = GetOriginalOrStem(list(f)[0])
        feat2 = GetOriginalOrStem(list(f)[1])
        feat3 = GetOriginalOrStem(list(f)[2])
        for sent in [s for s in sentences if s.find(feat1) != -1 and s.find(feat2) != -1 and s.find(feat3) != -1]:
          i1 = sent.find(feat1)
          i2 = sent.find(feat2)
          i3 = sent.find(feat3)
          minIndex = min(i1, i2, i3)
          maxIndex = max(i1, i2, i3)
          temp = [i for i in [i1, i2, i3] if i > minIndex and i < maxIndex]
          if len(temp) > 0:
            midIndex = temp[0]
            if sent[minIndex:midIndex].count(' ') <= PruneWordDistance and sent[midIndex:maxIndex].count(' ') <= PruneWordDistance:
              compactness += 1
          if compactness == PruneCompactSupport:
            break
        if compactness >= PruneCompactSupport:
          pruned.append(f)
          
    else:
      print 'The PrunePF does not handle features of length: ', len(features[0])


  return pruned


def PruneRedundancy(singleFeatures, phraseFeatures, sentences):
  
  pruned = []
  for f in singleFeatures:

    of = GetOriginalOrStem(f)
    superFeats = []
    
    for pf in phraseFeatures:        
      #TODO consider the case of gesture control and gesture-control and gesturecontrol: gesturecontrol is redundant of [gesture, control]
      if f in pf:
        superFeats.append(pf)
    if len(superFeats) == 0:
      pruned.append(f)

    else:
      count = 0
      otherFeats = reduce(lambda x, y: list(x) + list(y), superFeats)
      otherFeats = list(otherFeats)
      otherFeats = list(set(otherFeats))
      otherFeats.remove(f)      
      for sent in sentences:
        if sent.find(of) != -1 and not any(map(lambda x: sent.find(GetOriginalOrStem(x)) != -1, otherFeats)):
          count += 1
    
        if count == PruneRedundancyPSupport:
          break
      if count >= PruneRedundancyPSupport:
        pruned.append(f)

  return pruned

def PointwiseMutualInformationPruning(itemName, features):
    pmiList = {}
    #do a google search for itemName
    gUrl = URL('http://google.com')
    #spoofing
    gUrl.open(user_agent='Mozilla/5.0')
    nameHits = -1
    for f in features:
        #do a google search for f and get results count
        fHits = -1
        #do a google search for "f" "itemName"
        sumHits = -1
        pmi = sumHits/(nameHits * fHits)
        pmi = abs(pmi)
        pmiList[f] = pmi
    #do frequency distribution and multiply them by 3 and add to their frequences in class meronyms to come up with the final pruning score
    prunedFeatures = set()

    return prunedFeatures

def SementicPatternPruning(features, discriminators):
    prunedFeatures = set()

    return prunedFeatures

def FinalPruning(features, discriminators):
    mipPruned = PointwiseMutualInformationPruning(features)
    spPruned = SementicPatternPruning(features, discriminators)

    return list(mipPruned.intersection(spPruned))

features = []
featuresSets = []

def getItemFeatures(className, itemName,reviews):

  Stuff['reviews'] = reviews
    
  for r in Stuff['reviews']:
  
    rns = []
    sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
    sents = sent_tokenizer.tokenize(r)
  
    for sent in sents:
      Sentences.append(sent.lower())
      tokens = nltk.word_tokenize(sent.lower())
      tokens = [t for t in tokens if t.find('http') < 0 and len(t) > 2]
      taggedSent = nltk.pos_tag(tokens)
      #print 'The sentence is: ', sent
      #print 'The tokenized sentence is: ', tokens
      #print 'The tagged sentence is: ', taggedSent
      e = 0
      
      while e < len(taggedSent):
        if taggedSent[e][1].startswith('NN'):
          n = taggedSent[e][0]
          rns.append(n)
        e += 1
    
      processed_rns = Pre_Processor(rns)
      featuresSets.append(set(processed_rns))
      features.extend(set(processed_rns))
      rns = []

#print 'Num of featuresSets: ', len(featuresSets)
#print 'Num of features: ', len(features)

  fd = nltk.FreqDist(features)

  SubItemsets = [ f for s in featuresSets for f in s if Decimal(fd[f])/Decimal(len(featuresSets)) < MinSup]
  SubItemsets = list(set(SubItemsets))
#print 'SubItemSets are: ', len(SubItemsets)

  tempL1 = [ f for s in featuresSets for f in s if Decimal(fd[f])/Decimal(len(featuresSets)) >= MinSup]
  fd2 = nltk.FreqDist(tempL1)

  tempL1 = list(set(tempL1))

  L1 = [[f] for f in tempL1]
#print 'fd2: ', fd2
  singleFeatures = [ key for key in fd.keys() if fd2[key] >= OneWordFeaturesPSupport]

  C2 = Apriori_Gen(L1)

#print 'C2: ', len(C2)

  L2 = subSupEliminator(C2, featuresSets, MinSup)
#print 'L2 len: ', len(L2)
  #print L2

  C3 = Apriori_Gen(L2)
#print 'C3 len: ', len(C3)

  L3 = subSupEliminator(C3, featuresSets, MinSup)
  L3 = PrunePF(L3, Sentences)
#print 'L3 len: ', len(L3)
#print 'Pruned L3 ',L3


  #Do pruning for L2 now
  L2 = PrunePF(L2, Sentences)
#print 'Pruned L2: ', L2

#print 'SingleFeatures: ', singleFeatures
#print 'One-word features after reducancy pruning: ', PruneRedundancy(singleFeatures, L2 + L3, Sentences)

  singleFeatures = PruneRedundancy(singleFeatures, L2 + L3, Sentences)
  print 'Single features before PMI: ', singleFeatures
  mero = Meronyms(className)
  meronyms = mero.GetMeronyms()
  featuresInMeronyms = [f for f in singleFeatures if f in meronyms]
  featuresNotInMeronyms = set(singleFeatures).difference(featuresInMeronyms)
  pmi = PMIPruner()
  #TODO get full feature text before doin the pruning below using the altered GetOriginalOrStem fx..
  pmiPrunedFeatures = pmi.Prune(itemName, list(featuresNotInMeronyms))
  if 'pmiScrewed' not in pmiPrunedFeatures:
      singleFeatures = featuresInMeronyms + pmiPrunedFeatures
      print 'pmi Pruning worked..'
  else:
      print 'PMI Pruning didnt work..'
  print 'Single features after meronyms and pmi: ', singleFeatures
  LStr = []
  
  for l in L2 + L3:
    l = list(l)
    nn = ' '.join(x for x in l)
    LStr.append(nn)
    
  allFeatures = []
  allFeatures.extend(singleFeatures)
  allFeatures.extend(LStr)
        
  return allFeatures

'''

tagged = []
nps = []

for r in Stuff['reviews']:
 #print 'review:', r
  rns = []
  sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
  sents = sent_tokenizer.tokenize(r)
  for i in sents:
      #print 'Sentence:', i
      tokenized =  nltk.word_tokenize(i.lower())
      taggedSent = nltk.pos_tag(tokenized)
      #print taggedSent
      ns = []
      e = 0
      while e < len(taggedSent):
        if taggedSent[e][1].startswith('NN'):
           n = taggedSent[e][0]
           temp = e+1
           if temp < len(taggedSent):
             if taggedSent[temp][1].startswith('NN'):
               n += ' ' + taggedSent[temp][0]
               e = temp
               temp = temp + 1
           if temp < len(taggedSent):
              if taggedSent[temp][1].startswith('NN'):
                n += ' '  + taggedSent[temp][0]
                e = temp
           
           #print n
           ns.append(n)
       
        e += 1
      rns.extend(set(ns))
  
 #print 'features in the review: ', rns
  nps.extend(set(rns))

fd = nltk.FreqDist(nps)

longFeat = [key for key in fd.keys() if key.find(' ') != -1]

print 'Num of all features: ', len(nps)
print 'Num of unique features: ', len(set(nps))

minSupportFeatures = [ key for key in fd.keys() if Decimal(fd[key])/Decimal(len(Stuff['reviews'])) > .14]
maxNumFeatures = 12
    
for f in minSupportFeatures[:maxNumFeatures]:
  print f, fd[f]

print [ (key, fd[key]) for key in fd.keys() if ' ' in key ]

#for f in longFeat:
#if Decimal(fd[f]) > Decimal(len(Stuff['reviews'])) * .01:
#print f, fd[f]
    
'''

    
