from xgoogle.search import GoogleSearch, SearchError
from decimal import *
from nltk.corpus import swadesh
getcontext().prec = 4

class PMIPruner:
    def __init__(self):
        self.pmiList = {}
        self.nameHits = -1
        self.factor = 100000
        self.pruneCentage = 30
        self.ExcludeList = swadesh.words('en')
        self.ExcludeList.extend(['you', 'man', 'fly', 'anything', 'anyth','anyon', 'anyone', 'everything', 'everyth', 'something', 'someth', 'more', 'your', '&', 'any', 'the',
                                 'a', '|', '-', '_', 'thing', 'news', 'would', 'could', 'shall', 'day', 'year', 'week', 'peopl'])
    def Prune(self, itemName, feats):
        try:
            #print 'in try block..'
            #print 'After the engine definition..'
            print 'Passed features: ', feats
            features = [f for f in feats if f not in self.ExcludeList if f not in itemName.lower()]

            gs = GoogleSearch(itemName)
            gs.results_per_page = 10
            rs = gs.get_results()
            try:
                nameHits = Decimal(int(gs.num_results)/self.factor)
            except:
                print 'nameHits is screwed...'
                nameHits = 0
            print 'after searchin namHits: ', gs.num_results
            print 'NameHits for ', itemName, ' are: ', nameHits
            for f in features:
                pmi = 0
                gs = GoogleSearch(f)
                gs.results_per_page = 10
                rs = gs.get_results()
                try:
                    fHits = Decimal(int(gs.num_results)/self.factor)
                except:
                    fHits = 0
                #print 'fHits for: ', f, fHits
                geth = itemName + ' ' + f
                gs = GoogleSearch(geth)
                gs.results_per_page = 10
                rs = gs.get_results()
                try:
                    togetherHits = Decimal(int(gs.num_results)/self.factor)
                except:
                    togetherHits = 0
                #print 'together hits are: ', togetherHits
                if fHits != 0 and nameHits != 0:
                    pmi = togetherHits / (nameHits * fHits)
                    pmi = abs(pmi)
                #print 'f pmi: ', f, pmi * self.factor
                self.pmiList[f] = float(pmi * self.factor)
            sortee = sorted(self.pmiList, key= lambda pmi: self.pmiList[pmi], reverse=True)
            upLimit = 40
            if len(self.pmiList) > 100:
                upLimit = self.pruneCentage
            else:
                upLimit = len(self.pmiList) * self.pruneCentage/100
            pruned = sortee[0:upLimit]
            #print 'Pruned features are: ', pruned
            #print 'Rejected features are: ', sortee[upLimit: len(sortee)]
        except SearchError, e:
            print 'SearchEngine Error: %s' % e
            self.pmiList['pmiScrewed'] = True
            return self.pmiList
        print 'Pruned features are: ', pruned
        return pruned


