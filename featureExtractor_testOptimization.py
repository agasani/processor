import nltk
from decimal import *
import re
#from pattern.web import URL
from pmiPruner import PMIPruner
from classMeronyms import Meronyms
from nltk.corpus import swadesh
from multiprocessing import Pool
import mongoDataLayer
import time
import sentencement
import sentiment
from math import sqrt

data = mongoDataLayer.MongoDataLayer('Phones')

porter = nltk.PorterStemmer()
getcontext().prec = 3

MinSup = Decimal(.01)
features = []
featuresSets = []
SubItemsets = []
Sentences = []
#StemsDict = {}
#StemsAndActualsList = []
PruneCompactSupport = 2
PruneWordDistance = 4
PruneRedundancyPSupport = 3
OneWordFeaturesPSupport = 2
Stuff = {'className': 'Test'}
ExcludeList = swadesh.words('en')
ExcludeList.extend(['you', 'man', 'fly', 'anything', 'everything', 'something', 'someone', 'more', 'your', '&', 'any', 'the', 'news', 'a', '|', '-', '_', 'thing'])
ExcludeList = list(set(ExcludeList))
ExcludeList = [porter.stem(e.lower()) for e in ExcludeList]
sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
word_tokenizer = nltk.word_tokenize
pos_tagger = nltk.pos_tag
mero = Meronyms('phone')
Meronyms = mero.GetMeronyms()
#Meronyms = []

def Apriori_Gen(L):
        c = []
        for itemset in L:
            is_index = L.index(itemset)
            for nxt_index in range(is_index+1, len(L)):
                for f in L[nxt_index]:
                    if f not in L[is_index]:
                        new_itemset = list(L[is_index])
                        new_itemset.append(f)
                        subSetitem = False
                        if len(new_itemset) > 2:
                            if set([new_itemset[0], new_itemset[1]]) in SubItemsets:
                                subSetitem = True
                            if set([new_itemset[0], new_itemset[2]]) in SubItemsets:
                              subSetitem = True
                            if set([new_itemset[1], new_itemset[2]]) in SubItemsets:
                                subSetitem = True
                        if set(new_itemset) not in c and not subSetitem:
                            c.append(set(new_itemset))
        ct = []
        for it in c:
            ct.append(list(it))
        return ct

def subSupEliminator(C, featuresSets, minSup):
        l = []
        for s in C:
            count = 0
            for seti in featuresSets:
                s = set(s)
                if s.issubset(seti):
                    count += 1
                #else:
                    #print 'This not accounted for: ', seti
            if Decimal(count)/Decimal(len(featuresSets)) >= minSup:
              l.append(s)
            else:
              SubItemsets.append(s)
        return l

    # TODO re-work the regex part to have a consistant ting

def Pre_Processor(listOfFeatures):
        processedFeatures = []
        StemsAndActualsList = []
        StemsDict = {}
        for feat in listOfFeatures:
            originalFeat = feat.lower()
            feat = feat.lower()
            parts = re.findall(r'^([a-zA-Z0-9]*?)([^a-zA-Z0-9]+)([a-zA-Z0-9]*)$', feat)
            if len(parts) > 0:
                lparts = list(parts[0])
                for part in lparts:
                    if re.search('[a-zA-Z]+', part):
                        partStem = porter.stem(part)
                        if partStem not in ExcludeList:
                            processedFeatures.append(partStem)
                            StemsDict[part] = partStem
                            StemsAndActualsList.append({'stem': partStem, 'feat': part})
            else:
                stem = porter.stem(feat)
                if stem not in ExcludeList:
                    processedFeatures.append(stem)
                    StemsDict[originalFeat] = stem
                    StemsAndActualsList.append({'stem': stem, 'feat': originalFeat})

        return {'ProcessedRNS': processedFeatures, 'StemsDict': StemsDict, 'StemsAndActualsList': StemsAndActualsList}

def GetOriginalOrStem(stem, stemsDict):
        originals = [key for key in stemsDict.keys() if stemsDict[key] == stem]
        toUse = [orig for orig in originals if len(orig) <= len(stem)]
        if len(toUse) > 0:
            if stem[len(stem)-1] != toUse[0][len(toUse[0])-1]:
                return stem[0: len(stem)-1]
            else:
                return stem
        else:
            return stem

def PrunePF(features, sentences, stemsDict):
        pruned = []
        if(len(features) > 0):
            if len(features[0]) == 2:
                for f in features:
                    compactness = 0
                    feat1 = GetOriginalOrStem(list(f)[0], stemsDict)
                    feat2 = GetOriginalOrStem(list(f)[1], stemsDict)
                    for sent in [s for s in sentences if s.find(feat1) != -1 and s.find(feat2) != -1]:
                        i1 = sent.find(feat1) if sent.find(feat1) < sent.find(feat2) else sent.find(feat2)
                        i2 = sent.find(feat1) if sent.find(feat1) > sent.find(feat2) else sent.find(feat2)
                        if sent[i1:i2].count(' ') <= PruneWordDistance:
                            compactness += 1
                        if compactness == PruneCompactSupport:
                            break
                    if compactness >= PruneCompactSupport:
                        pruned.append(f)
            elif len(features[0]) == 3:
              for f in features:
                  compactness = 0
                  feat1 = GetOriginalOrStem(list(f)[0], stemsDict)
                  feat2 = GetOriginalOrStem(list(f)[1], stemsDict)
                  feat3 = GetOriginalOrStem(list(f)[2], stemsDict)
                  for sent in [s for s in sentences if s.find(feat1) != -1 and s.find(feat2) != -1 and s.find(feat3) != -1]:
                      i1 = sent.find(feat1)
                      i2 = sent.find(feat2)
                      i3 = sent.find(feat3)
                      minIndex = min(i1, i2, i3)
                      maxIndex = max(i1, i2, i3)
                      temp = [i for i in [i1, i2, i3] if i > minIndex and i < maxIndex]
                      if len(temp) > 0:
                          midIndex = temp[0]
                          if sent[minIndex:midIndex].count(' ') <= PruneWordDistance and sent[midIndex:maxIndex].count(' ') <= PruneWordDistance:
                              compactness += 1
                      if compactness == PruneCompactSupport:
                          break
                  if compactness >= PruneCompactSupport:
                      pruned.append(f)
            else:
                print('The PrunePF does not handle features of length: ', len(features[0]))
        return pruned

def PruneRedundancy(singleFeatures, phraseFeatures, sentences, stemsDict):
        pruned = []
        for f in singleFeatures:
            of = GetOriginalOrStem(f, stemsDict)
            superFeats = []
            for pf in phraseFeatures:
              #TODO consider the case of gesture control and gesture-control and gesturecontrol: gesturecontrol is redundant of [gesture, control]
              #TODO continued Pre_Processor turns gesture-control and 'gesture control' into gesturecontrol that way we have same feature for all 3 variations
              #TODO continued if done as described above how do I get phrases where such feature is in? using originalFeat would be the ideal way to do it; check and mk sure that's how it's done..
                if f in pf:
                    superFeats.append(pf)
            if len(superFeats) == 0:
                 pruned.append(f)
            else:
                count = 0
                otherFeats = reduce(lambda x, y: list(x) + list(y), superFeats)
                otherFeats = list(otherFeats)
                otherFeats = list(set(otherFeats))
                otherFeats.remove(f)
                for sent in sentences:
                    if sent.find(of) != -1 and not any(map(lambda x: sent.find(GetOriginalOrStem(x), stemsDict) != -1, otherFeats)):
                        count += 1
                    if count == PruneRedundancyPSupport:
                        break
                if count >= PruneRedundancyPSupport:
                    pruned.append(f)
        return pruned

def GetTheMostFrequentFeatureName(stem, allStemsAndActualsList):
        hvSameStemFeats = [f['feat'] for f in allStemsAndActualsList if f['stem'] == stem]
        frequ = nltk.FreqDist(hvSameStemFeats)
        return frequ.max()

def OrderFeaturesWords(feature, reviews):
        words = feature.split(' ')
        feat = ''
        '''for p in words:
            feat = feat + GetTheMostFrequentFeatureName(p, StemsAndActualsList) + ' ' '''
        if len(words) == 2:
            w1 = words[0]
            w2 = words[1]
            indexes1 = [r.index(w1) for r in reviews if w1 in r and w2 in r][0:10]
            indexes2 = [r.index(w2) for r in reviews if w1 in r and w2 in r][0:10]
            if sum(indexes1) < sum(indexes2):
                feat = GetTheMostFrequentFeatureName(w1, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w2, StemsAndActualsList)
            else:
                feat = GetTheMostFrequentFeatureName(w2, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w1, StemsAndActualsList)

        elif len(words) == 3:
            w1 = words[0]
            w2 = words[1]
            w3 = words[2]
            indexes1 = [r.index(w1) for r in reviews if w1 in r and w2 in r and w3 in r][0:10]
            indexes2 = [r.index(w2) for r in reviews if w1 in r and w2 in r and w3 in r][0:10]
            indexes3 = [r.index(w3) for r in reviews if w1 in r and w2 in r and w3 in r][0:10]
            if sum(indexes1) < sum(indexes2):
                if sum(indexes2) < sum(indexes3):
                    feat = GetTheMostFrequentFeatureName(w1, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w2, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w3, StemsAndActualsList)
                else:
                    if sum(indexes3) > sum(indexes1):
                        feat = GetTheMostFrequentFeatureName(w1, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w3, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w2, StemsAndActualsList)
                    else:
                        feat = GetTheMostFrequentFeatureName(w3, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w1, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w2, StemsAndActualsList)
            else:
                if sum(indexes1) < sum(indexes3):
                    feat = GetTheMostFrequentFeatureName(w2, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w1, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w3, StemsAndActualsList)
                else:
                    if sum(indexes3) > sum(indexes2):
                        feat = GetTheMostFrequentFeatureName(w2, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w3, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w1, StemsAndActualsList)
                    else:
                        feat = GetTheMostFrequentFeatureName(w3, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w2, StemsAndActualsList) + ' ' + GetTheMostFrequentFeatureName(w1, StemsAndActualsList)
        return feat.strip()

def getFeaturesAndFeatureSets(r):
            rns = []
            sents = sent_tokenizer.tokenize(r)
            for sent in sents:
                Sentences.append(sent.lower())
                tokens = word_tokenizer(sent.lower())
                tokens = [t for t in tokens if t.find('http') < 0 and len(t) > 2]
                taggedSent = pos_tagger(tokens)
                e = 0
                while e < len(taggedSent):
                    if taggedSent[e][1].startswith('NN'):
                        n = taggedSent[e][0]
                        rns.append(n)
                    e += 1
                results = Pre_Processor(rns)
                processed_rns = results['ProcessedRNS']
                StemsDict = results['StemsDict']
                StemsAndActualsList = results['StemsAndActualsList']
                featuresSets.append(set(processed_rns))
                features.extend(set(processed_rns))
                rns = []
            return {'features': features, 'featuresSets': featuresSets, 'StemsDict': StemsDict, 'StemsAndActualsList': StemsAndActualsList}

def test(x):
        return x * 2

def getFeatsTest():
        '''pool  = Pool(processes=4)
        t = pool.map(test, range(10))'''
        t = []
        for i in t:
            print i
        print 'After running the multi-process the result is: ', t
        return 0

def getItemFeatures(itemName, reviews):
        print('getting features..')

        pool = Pool(processes=4)
        results = pool.map(getFeaturesAndFeatureSets, reviews)
        pool.close()
        features = [i for f in results for i in f['features']]
        featuresSets = [i for f in results for i in f['featuresSets']]
        #stemsAndActualsList = [i for f in results for i in f['StemsAndActualsList']]
        stemsAndActualsList = []
        testi = [t['StemsAndActualsList'] for t in results]
        for t in testi:
            stemsAndActualsList.extend(t)

        stemsDict = {}
        for r in results:
            for f in r['StemsDict']:
                stemsDict[f] = r['StemsDict'][f]

        print 'FEATURES: ', len(features)
        print 'FEATURESSETS: ', len(featuresSets)
        print 'STEMSDICT: ', len(stemsDict)
        print 'STEMSANDACTUALSLIST: ', len(stemsAndActualsList)

        if len(features) < 500:
            MinSup = Decimal(.03)
            print('MinSup changed to: ', MinSup)
        fd = nltk.FreqDist(features)
        SubItemsets = [ f for s in featuresSets for f in s if Decimal(fd[f])/Decimal(len(featuresSets)) < MinSup]
        SubItemsets = list(set(SubItemsets))
        tempL1 = [ f for s in featuresSets for f in s if Decimal(fd[f])/Decimal(len(featuresSets)) >= MinSup]
        fd2 = nltk.FreqDist(tempL1)
        tempL1 = list(set(tempL1))
        L1 = [[f] for f in tempL1]
        singleFeatures = [ key for key in fd.keys() if fd2[key] >= OneWordFeaturesPSupport]
        C2 = Apriori_Gen(L1)
        L2 = subSupEliminator(C2, featuresSets, MinSup)
        C3 = Apriori_Gen(L2)
        L3 = subSupEliminator(C3, featuresSets, MinSup)
        L3 = PrunePF(L3, Sentences, stemsDict)
        L2 = PrunePF(L2, Sentences, stemsDict)
        singleFeatures = PruneRedundancy(singleFeatures, L2 + L3, Sentences, stemsDict)
        meronyms = Meronyms
        featuresInMeronyms = [f for f in singleFeatures if f in meronyms]
        featuresNotInMeronyms = set(singleFeatures).difference(featuresInMeronyms)
        pmi = PMIPruner()
        pmiPrunedFeatures = pmi.Prune(itemName, list(featuresNotInMeronyms))

        if 'pmiScrewed' not in pmiPrunedFeatures:
            singleFeatures = featuresInMeronyms + pmiPrunedFeatures
            print('pmi Pruning worked..')
        else:
            print('PMI Pruning didnt work..')

        LStr = []
        for l in L2 + L3:
            l = list(l)
            nn = ' '.join(x for x in l)
            LStr.append(nn)
        print 'SINGLE FEATURES: ', singleFeatures
        stemmedFeatures = []
        stemmedFeatures.extend(singleFeatures)
        stemmedFeatures.extend(LStr)
        allFeatures = {}
        allStemsAndActualsList = stemsAndActualsList

        for s in stemmedFeatures:
            if ' ' not in s:
                allFeatures[s] = GetTheMostFrequentFeatureName(s, allStemsAndActualsList)
            else:
                allFeatures[s] = OrderFeaturesWords(s, reviews)

        return allFeatures

def getEntityRanking(pos, neg):
    total = pos + neg
    if total == 0:
        return 0
    z = 1.64485
    p = float(pos)/total
    confidence = (p+z*z/(2*total)-z*sqrt((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)
    print 'Before the normalization ting, confidence is: ', confidence
    return round(confidence * 5, 1)

def getSentiments(reviews, features, meronyms):
    sentiments = sentencement.getSentiments(reviews, features)
    return sentiments

def getPosandNegNum(reviews):
      pos = 0
      neg = 0
      for rev in reviews:
          try:
              if rev['rating'] in ['4', '5']:
                  pos = pos + 1
              elif rev['rating'] in ['1', '2']:
                  neg = neg + 1
              else:
                  content = ''
                  try:
                      content = rev['description']
                  except:
                      print 'The review has no description attr, then it should be content..'
                      content = rev['content']
                  polarity = sentiment.getReviewPolarity(content)
                  if polarity > 0:
                      pos = pos + 1
                  elif polarity < 0:
                      neg = neg + 1

          except:
              content = ''
              try:
                  content = rev['description']
              except:
                  content = rev
              polarity = sentiment.getReviewPolarity(content)
              if polarity > 0:
                pos = pos + 1
              elif polarity < 0:
                neg = neg + 1

      return {'pos': pos, 'neg': neg}

if __name__ == '__main__':
    reviews = data.getQuickTestReviews()
    numRev = len(reviews)
    name = 'sumsang galaxy s4'
    print 'len reviews is: ', numRev

    initClockTime = time.clock()
    initWallTime = time.time()
    featuresAndStems = getItemFeatures(name, reviews)
    print 'GETTING FEATURES TOOK: ', time.clock() - initClockTime, ' CLOCK TIME FOR THE PROCESS TO FINISH'
    print 'GETTING FEATURES TOOK: ', time.time() - initWallTime, ' WALL TIME FOR THE PROCESS TO FINISH'
    featureWords = featuresAndStems.keys()
    #print 'features stems: ', featureWords
    print 'actual features: ', featuresAndStems.values()

    initClockTime = time.clock()
    initWallTime = time.time()
    sentiments = getSentiments(reviews, featureWords)
    print 'GETTING SENTIMENTS TOOK: ', time.clock() - initClockTime, ' CLOCK TIME FOR THE PROCESS TO FINISH'
    print 'GETTING SENTIMENTS TOOK: ', time.time() - initWallTime, ' WALL TIME FOR THE PROCESS TO FINISH'

    itemScore = 0.0
    posNneg = getPosandNegNum(reviews)
    itemScore = getEntityRanking(posNneg['pos'], posNneg['neg'])
    print 'itemScore: ', itemScore
