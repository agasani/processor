#from pattern.web import Google
from xgoogle.search import GoogleSearch, SearchError
from decimal import *
import math
from time import sleep

getcontext().prec = 4

itemName = 'Blackberry Bold'
features = ["anything", "app", "app blackberry", "app iphone", "battery", "battery life", "blackberry", "bold", "browser", "busy", "button", "call", "camera", "camera picture", "community", "company", "contact", "curve","day",
  "design", "device", "display", "email", "everything", "excellent", "feature", "feel", "focus", "function", "game", "get", "great", "hand", "issue", "keyboard", "life", "line", "look", "lot", "market", "model", "month",
  "people", "person", "phone", "phone app", "phone blackberry", "phone busy", "phone day", "phone email", "phone iphone", "phone keyboard", "phone screen", "phone time", "picture", "play", "playbook", "problem",
  "processor", "product", "quality", "qwerti", "rim", "screen", "screen keyboard", "service", "smartphone", "software", "someone", "something", "speed", "sprint", "text", "thing", "thing phone", "time", "touch",
  "touch keyboard", "touch screen", "touch screen keyboard", "use", "user", "version", "video", "way", "web", "week", "work", "world", "year"]

pmiList = {}
nameHits = -1
factor = 1000000
pmiList['pmiScrewed'] = False
denominators = [1, 10,100,1000, 10000, 100000, 1000000]
#do a google search for itemName
#gUrl = URL('http://google.com')
#spoofing
#gUrl.open(user_agent='Mozilla/5.0')
try:
    print 'in try block..'
    #engine = DuckDuckGo(license=None, throttle=0.5, language='en')
    print 'After the engine definition..'
    #nameHits = Decimal(engine.search(itemName).total/factor)
    gs = GoogleSearch(itemName)
    gs.results_per_page = 10
    rs = gs.get_results()
    nameHits = Decimal(int(gs.num_results)/factor)
    print 'after searchin namHits'
    print 'NameHits are: ', nameHits
    for f in features:
        #do a google search for f and get results count
        #sleep(2)
        pmi = 0
        #fHits = Decimal(engine.search(f).total/factor)
        gs = GoogleSearch(f)
        gs.results_per_page = 10
        rs = gs.get_results()
        fHits = Decimal(int(gs.num_results)/factor)
        print 'fHits for: ', f, fHits
        geth = itemName + ' ' + f
        #togetherHits = Decimal(engine.search(geth).total/factor)
        gs = GoogleSearch(geth)
        gs.results_per_page = 10
        rs = gs.get_results()
        togetherHits = Decimal(int(gs.num_results)/factor)
        print 'together hits are: ', togetherHits
        #do a google search for "f" "itemName"
        if fHits != 0:
            pmi = togetherHits / (nameHits * fHits)
            pmi = abs(pmi)
        print 'f pmi: ', f, pmi * factor
        pmiList[f] = float(pmi * factor)
    #normalize the pmi to ten
    maxim = max(pmiList.values())
    denom = 1
    for d in denominators:
        if maxim/d < 10:
            denom = d
            break
    if denom > 1:
        for p in pmiList:
            pmiList[p] = pmiList[p]/denom
    print 'pmiList after normalizing to 10: ', pmiList
    sortee = sorted(pmiList, key= lambda pmi: pmiList[pmi], reverse=True)
    upLimit = 40
    if len(pmiList) > 100:
        upLimit = 40
    else:
        upLimit = len(pmiList) * 30/100
    pruned = sortee[0:upLimit]
    print 'Pruned features are: ', pruned
    print 'Rejected features are: ', sortee[upLimit: len(sortee)]
except SearchError, e:
    print 'SearchEngine Error: %s' % e
    pmiList['pmiScrewed'] = True




#do frequency distribution and multiply them by 3 and add to their frequences in class meronyms to come up with the final pruning score
prunedFeatures = set()


