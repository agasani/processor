import nltk
from decimal import *
import re
import sentimentTokenizer as Tokenizer
#import json
#import random
import pickle
#from nltk.corpus import movie_reviews

'''from pymongo import MongoClient
    
client = MongoClient();
db = client.ReviewizeDB;
testData = db.testData;'''

getcontext().prec = 2
Sentence_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
Tokenizer = Tokenizer.Tokenizer(preserve_case=False)

CountProgress = 0

posFile = 'Resources/positive-words.txt'
negFile = 'Resources/negative-words.txt'
testDataFile = 'Resources/testReviews.txt'
f = open('Resources/NBClassifierV1.pickle')
NBClassifier = pickle.load(f)
f.close()
f = open('Resources/MEClassifierV1.pickle')
#MEClassifier = pickle.load(f)
f.close()


try:
    with open(posFile, 'r') as pf:
        posWords = pf.read()
        posWords = posWords.split('\n')
except ValueError:
    print 'Could not open: ', posFile
    posWords= []

try:
    with open(negFile, 'r') as nf:	
        negWords = nf.read()
        negWords = negWords.split('\n')
except ValueError:
    print 'Could not open: ', negFile
    negWords= []

'''try:
    with open(testDataFile, 'r') as tf:
        rs = tf.read()
        testReviews = json.loads(rs)
        print 'Test Reviews are: ', len(testReviews)
        #testData.insert(testReviews)
except ValueError:
   print 'Could not open: ', testDataFile
   testReviews = []'''

def getWordPolarity(word):
    
    sentiment = 0
    
    try:
        if word.encode('ascii', 'ignore') in negWords:
            sentiment = -1
    except:
        sentiment = sentiment
    
    try:
        if word.encode('ascii', 'ignore') in posWords:
            sentiment = 1
    except:
        sentiment = sentiment
    
    return sentiment

def negateWord(word):
    
    try:
        negWords.index(word)
        word = word + '_NEG'
    except:
        word = word
    
    return word

#When there is a verb in negation with an adjective it gets twisted, sometime the adjective is negated when it shouldnt
# e.g I didnt like the ugly scene. => ugly true value is -1 but coz it's after didnt it gets a +1 and sentence tends to be pos..
# need a fix for this. I only works with verbs like 'to be', 'become' ...

def getReviewSimplePolarity(review):
    
    #print 'tokenized input: ', reviewWords
    reviewWords = Tokenizer.tokenize(review)
    score = 0
    negate = False
    
    for w in reviewWords:
        if re.search(r"(?:^(?:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint)$)|n't", w):
            negate = True
        if re.match(r"^[.:;!?]$", w):
            negate = False
        
        if negate == False:
            score = score + getWordPolarity(w)
        elif negate == True:
            score = score - getWordPolarity(w)
    
    return score

def applyNegation(review):
    reviewWords = Tokenizer.tokenize(review)
    tags = nltk.pos_tag(reviewWords)
    tagList = {}
    for tag in tags:
        tagList[tag[0]] = tag[1]
    processed = []
    negate = False
          
    for w in reviewWords:
        if re.search(r"(?:^(?:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint)$)|n't", w):
            negate = True
            continue
        if re.match(r"^[.:;!?]$", w):
            negate = False
            continue
        if negate == False:
            if tagList[w].startswith('JJ') or tagList[w].startswith('VB'):# or tagList[w].startswith('RB'):
                processed.append(w)
        elif negate == True:
            if tagList[w].startswith('JJ') or tagList[w].startswith('VB'):# or tagList[w].startswith('RB'):
                processed.append(w + '_NEG')

    return processed

def getReviewFeatures(review):
    features = {}
    sentences = Sentence_tokenizer.tokenize(review)
    #print 'Sentences len: ', len(sentences)
    psp, nsp = 0.0, 0.0 #psp: Positive Sentence Percentage nps: Negative Sentence Percentage
    for sent in sentences:
        sentPolarity = getReviewSimplePolarity(sent)
        if sentPolarity == 1:
            psp = psp + 1
        elif sentPolarity == -1:
            nsp = nsp + 1

    if len(sentences) > 0:
        features['psp'] = int((Decimal(psp)/Decimal(len(sentences))) * 100)
        features['nsp'] = int((Decimal(nsp)/Decimal(len(sentences))) * 100)
    else:
        features['psp'] = 0
        features['nsp'] = 0
    #features['polarity'] = getReviewSimplePolarity(review)
    adjNverbsWitNeg = applyNegation(review)

    #for word in adjNverbsWitNeg:
    #   features['contains(%s)' % word] = (word in adjNverbsWitNeg)

    for word in adjNverbsWitNeg:
        if '_NEG' in word:
            temp = word[0:word.index('_NEG')]
            if temp.encode('ascii', 'ignore') in posWords:
                features['(%s).isPos' % temp] = False
            elif temp.encode('ascii', 'ignore') in negWords:
                features['(%s).isNeg' % temp] = False
        else:
            if word.encode('ascii', 'ignore') in posWords:
                features['(%s).isPos' % word] = True
            elif word.encode('ascii', 'ignore') in negWords:
                features['(%s).isNeg' % word] = True

    '''global CountProgress
    CountProgress = CountProgress + 1
    print 'Progress: ', CountProgress'''
    return features

'''negReviews = testData.find({'starRating': 1})
posReviews = testData.find({'starRating': 5})

neg = [{'content': rev['content'], 'starRating': rev['starRating']} for rev in negReviews]
pos = [{'content': rev['content'], 'starRating': rev['starRating']} for rev in posReviews]'''


'''    
f = open('posReviews.pickle')
posReviews = pickle.load(f)
f.close()

f = open('negReviews.pickle')
negReviews = pickle.load(f)
f.close()

print 'Positive reviews: ', len(posReviews)#.count(with_limit_and_skip=True)
print 'Negative reviews: ', len(negReviews)#.count(with_limit_and_skip=True)

featuresSets = [(getReviewFeatures(rev['content']), 'pos') for rev in posReviews] + [(getReviewFeatures(rev['content']), 'neg') for rev in negReviews]

random.shuffle(featuresSets)

size = int(len(featuresSets) * 0.12)
trainSet, testSet = featuresSets[size:], featuresSets[:size]
    
print 'Train size: ', len(trainSet)
print 'Test size: ', len(testSet)

NBClassifier = nltk.NaiveBayesClassifier.train(trainSet)
MEClassifier = nltk.MaxentClassifier.train(trainSet, algorithm='iis')

print 'NB Accuracy: ', nltk.classify.accuracy(NBClassifier, testSet) * 100, '%'
print 'ME Accuracy: ', nltk.classify.accuracy(MEClassifier, testSet) * 100, '%'

print 'NB Most important features: ', NBClassifier.show_most_informative_features(10)
print 'ME Most important features: ', MEClassifier.show_most_informative_features(10)

print 'contains only adjand verbs with negation, with psp, nsp, without polarity, using Neg and Pos words from Ping liu separately'
    
f = open('NBClassifierV1.pickle', 'wb')
pickle.dump(NBClassifier, f)
f.close()

f = open('MEClassifierV1.pickle', 'wb')
pickle.dump(MEClassifier, f)
f.close()
'''

#TODO next is doin testing wit wat I hv so far and how it performs then add POS and Parsing and compare the performance...
'''
    Sentiment classifier Features List:
        - Word polarity(Negation)
        - Positive words Percentence (incorporated in review polarity)
        - Unigram(this the contains word feature)
        - Positive Sentences Percentage and Negative Sentences Percentage (on review level use polarity of the sentence to determine weather it's positive)
        - Do PSP and NSP using sentiwordnet instead of using Ping Liu Lexicon for neg&pos and psp and nsp
        - * POS
        - * Dependence Parsing
        - * Sementic Parsing
    '''

#TODO train another classifier with more and diverse corpus to improve the current one..
#TODO 'continued' or see if I can use off-the-shelve sentiment classifier, for now ima use the NBClassifier..

'''
    documents = [(list(movie_reviews.words(fileid)), category)
    for category in movie_reviews.categories()
    for fileid in movie_reviews.fileids(category)]
    
    random.shuffle(documents)
    
    #featuresets = [(getReviewFeatures(' '.join(str(x) for x in d)), c) for (d,c) in documents]
    
    #train_set, test_set = featuresets[100:], featuresets[:100]
    #classifier = nltk.NaiveBayesClassifier.train(train_set)
    
    featuresets = [(getReviewFeatures(' '.join(str(x) for x in d)), c) for (d,c) in documents[:300]]
    
    print 'NB Accuracy: ',nltk.classify.accuracy(NBClassifier, featuresets)
    print 'ME Accuracy: ',nltk.classify.accuracy(MEClassifier, featuresets)
    
    print 'NB: ', NBClassifier.show_most_informative_features(10)
    print 'ME: ', MEClassifier.show_most_informative_features(10)
    '''

def getReviewPolarity(review):
    features = getReviewFeatures(review)
    polarity = NBClassifier.classify(features)

    if polarity == 'pos':
        return 1
    else:
        return -1

if __name__ == '__main__':
    #review = raw_input('Enter your review: ')
    review = 'Punishingly long, the movie insists that viewers spend more than two over-deliberate hours with a man whose pathology could be sketched in a few minutes. In sports, what Foxcatcher does is called "running out the clock.'
    pol = getReviewPolarity(review)
    print 'The review Polarity is: ', pol


