__author__ = 'agasani'
from pythia import LexRankSummarizer
from pythia import CentroidSummarizer
import mongoDataLayer
import nltk
import datetime
import re
import random, time
#import language_check

class MyRand(object):
    def __init__(self):
        self.last = None
        self.conjWords = ['But', 'On the contrary', 'On the other hand', 'Yet', 'Though', 'Anyhow', 'Nevertheless', 'Still', 'However']
        self.TopLimit = len(self.conjWords) - 1

    def __call__(self):
        r = random.randint(0, self.TopLimit)
        while r == self.last:
            r = random.randint(0, 9)
        self.last = r
        return self.conjWords[r]


class Summarizer:

    def __init__(self):
        self.data = mongoDataLayer.MongoDataLayer('phones')
        self.word_tokenizer = nltk.word_tokenize
        self.matchStemRXPattern = '[a-zA-Z]{0,4}'
        self.StopWords = ['.', ':', ';', '!', '?', '...', '..']
        self.word_tokenizer = nltk.word_tokenize
        self.pos_tagger = nltk.pos_tag
        self.conjWords = ['But', 'On the contrary', 'On the other hand', 'Yet', 'Though', 'Anyhow', 'Nevertheless', 'Still', 'However']
        self.minTokensInValidPhrase = 5
        self.posPhrasesNum = 4
        self.negPhrasesNum = 3
        #self.LexRank = LexRankSummarizer()
        #self.Centroid = CentroidSummarizer()
        #self.langTool = language_check.LanguageTool('en-US')

    def rankPhrases(self, phrases):
        try:
            LexRank = LexRankSummarizer(phrases)
            rankedPhrases = LexRank.summarize()
        except Exception as ex:
            print 'Shit happened: ', ex.message
            return []

        return rankedPhrases

    def summarize(self, phrases, features, threshold=3):
        rankedPhrases = self.rankPhrases(phrases)
        #print 'RANKED LEN: ', len(rankedPhrases)
        #for ranked in rankedPhrases[0:12]:
            #print 'DIST: ', ranked['dist']
            #print 'TEXT: ', ranked['raw']
            #print '\n'

        if threshold == 4:
            print 'Its POS'
        elif threshold == 3:
            print 'Its NEG'

        summaryPhrases = []
        addedFeats = []
        firstRoundRejectsPhrases = []

        print 'FEATURES ARE: ', features

        #print 'RankedPhrases: ', [p['raw'] for p in rankedPhrases]
        for phrase in rankedPhrases:

            if len(summaryPhrases) == threshold:
                break

            addPhrase = False

            phraseFeats = []
            #print ''
            #print 'Right BEFORE the features for-loop..'
            #print 'add Phrase is: ', addPhrase
            #print 'addedFeats: ', addedFeats
            #print 'And the phrase is: '
            #print phrase['raw']

            for feat in features:
                '''if feat in addedFeats:
                    addPhrase = False
                    print 'About to break out and the feature is: ', feat'''
                featz = feat.split(' ')
                if len(featz) == 1:
                    f1Search = re.search(r'\b' + featz[0] + self.matchStemRXPattern + r'\b', phrase['raw'])
                    if f1Search and feat not in addedFeats and phrase['raw'] not in summaryPhrases:
                        phraseFeats.append(feat)
                        addPhrase = True
                        #print ''
                        #print 'addPhrase just turned: ', addPhrase
                        #print 'FEAT is: ', feat
                    elif f1Search and feat in addedFeats:
                        addPhrase = False
                        #print ''
                        #print 'addPhrase just turned: ', addPhrase
                        #print 'FEAT is: ', feat
                        break
                elif len(featz) == 2:
                    f1Search = re.search(r'\b' + featz[0] + self.matchStemRXPattern + r'\b', phrase['raw'])
                    f2Search = re.search(r'\b' + featz[1] + self.matchStemRXPattern + r'\b', phrase['raw'])
                    if f1Search and f2Search and feat not in addedFeats and phrase['raw'] not in summaryPhrases:
                        phraseFeats.append(feat)
                        addPhrase = True
                    elif f1Search and f2Search and feat in addedFeats:
                        addPhrase = False
                        break
                elif len(featz) == 3:
                    f1Search = re.search(r'\b' + featz[0] + self.matchStemRXPattern + r'\b', phrase['raw'])
                    f2Search = re.search(r'\b' + featz[1] + self.matchStemRXPattern + r'\b', phrase['raw'])
                    f3Search = re.search(r'\b' + featz[2] + self.matchStemRXPattern + r'\b', phrase['raw'])
                    if f1Search and f2Search and f3Search and feat not in addedFeats and phrase['raw'] not in summaryPhrases:
                        phraseFeats.append(feat)
                        addPhrase = True
                    elif f1Search and f2Search and f3Search and feat in addedFeats:
                        addPhrase = False
                        break

            #print ''
            #print 'Right AFTER the features for-loop'
            #print 'add Phrase is: ', addPhrase
            #print 'addedFeats: ', addedFeats
            #print 'And the phrase is: '
            #print phrase['raw']
            #print 'AND summaryPhrases are: ', summaryPhrases

            if addPhrase:
                tokens = self.word_tokenizer(phrase['raw'])

                #make sure if a phrase has less than validNumTokens words doesnt get added the first time
                if len(tokens) < self.minTokensInValidPhrase:
                    addPhrase = False
                    firstRoundRejectsPhrases.append(phrase['raw'])
                tagged = self.pos_tagger(tokens)
                #dont add phrases with 'I' as subject, I dont want people talking bout their personal xperience and shit
                for tag in tagged:
                    if tag[1] == 'PRP' and tag[0].lower() == 'i':
                        addPhrase = False
                        firstRoundRejectsPhrases.append(phrase['raw'])
                        break
                        #print 'PHRASE TO NOT BE ADDED: ', phrase['raw']
                if addPhrase:
                    ph = phrase['raw']

                    '''
                    matches = self.langTool.check(ph)
                    if len(matches) > 0:
                        print ''
                        print 'The Phrase has some ERRORS:'
                        print 'Incorrect Phrase: ', ph
                        print 'Correct phrase: ', language_check.correct(ph, matches)
                        print ''
                    '''

                    summaryPhrases.append(ph)
                    addedFeats.extend(phraseFeats)
                    #print ''
                    #print 'ABOUT TO ADD FEATURE..'
                    #print 'addPhrase is: ', addPhrase
                    #print 'addedFeats: ', addedFeats
                    #print 'phraseFeats: ', phraseFeats
                    #print 'And the phrase is: '
                    #print ph
            else:
                firstRoundRejectsPhrases.append(phrase['raw'])
        #add the first n phrases from firstRoundRejectsPhrases if summaryPhrase is < threshold and rejects > 0
        if len(summaryPhrases) < threshold:
            for idx in range(0, len(firstRoundRejectsPhrases)):
                p = firstRoundRejectsPhrases[idx]
                if p not in summaryPhrases:
                    summaryPhrases.append(p)
                if len(summaryPhrases) >= threshold:
                    break

        summary = ''
        for s in summaryPhrases:
            s = s.strip()
            if s[len(s) - 1] in self.StopWords:
                s = s
            else:
                s = s.strip(' ') + '...'
            if len(summary) == 0:
                summary = s.capitalize()
            elif summary[len(summary) - 1] in self.StopWords:
                summary = summary.strip(' ') + ' ' + s.capitalize()
            else:
                summary = summary.strip(' ') + '... ' + s.capitalize()
        return summary

    def start(self, features):
        print 'IN SUMMARIZER..'
        posFeatures = []
        negFeatures = []
        posPhrases = {}
        negPhrases = {}
        for idx, feat in enumerate(features):
            if feat['score'] > 0:
                posFeatures.append(feat['stem'])
                for i, rev in enumerate(feat['pos']):
                    tokens = self.word_tokenizer(rev['text'].lower())
                    index = str(idx) + str(i)
                    posPhrases[index] = {'tokens': tokens, 'raw': rev['text'].lower(),
                                         'word_frequencies': nltk.FreqDist(tokens).items()}
            elif feat['score'] < 0:
                negFeatures.append(feat['stem'])
                for i, rev in enumerate(feat['neg']):
                    tokens = self.word_tokenizer(rev['text'].lower())
                    index = str(idx) + str(i)
                    negPhrases[index] = {'tokens': tokens, 'raw': rev['text'].lower(),
                                         'word_frequencies': nltk.FreqDist(tokens).items()}
        posFeatures.reverse()
        if len(posPhrases) > 0:
            posSummary = self.summarize(posPhrases, posFeatures, threshold=self.posPhrasesNum).strip(' ')
        else:
            posSummary = ''
        if len(negPhrases) > 0:
            negSummary = self.summarize(negPhrases, negFeatures, threshold=self.negPhrasesNum).strip(' ')
        else:
            negSummary = ''
        posSummary = posSummary.strip(' ')
        negSummary = negSummary.strip(' ')
        #topLimit = len(self.conjWords) - 1
        #randint = MyRand()
        #conj = randint()
        random.seed(time.clock())
        rand = random.randint(0, (len(self.conjWords) - 1))
        conj = self.conjWords[rand]
        print 'RANDOM CONJ IS: ', conj
        if len(posSummary) > 0 and len(negSummary) > 0:
            addConjunction = True
            for c in self.conjWords:
                if c.lower() in negSummary.lower():
                    addConjunction = False
            if addConjunction:
                negSummary = negSummary[0].lower() + negSummary[1:]
                summary = posSummary + ' ' + conj.capitalize() + ', ' + negSummary
            else:
                summary = posSummary + ' ' + negSummary.capitalize()
        elif len(posSummary) > 0:
            summary = posSummary
        elif len(negSummary) > 0:
            summary = negSummary
        else:
            summary = ''
        print 'SUMMARY IS: '
        print summary
        return summary

if __name__ == '__main__':
    summarizer = Summarizer()
    word_tokenizer = nltk.word_tokenize
    testEntity = summarizer.data.getProductForTest(name='iphone 5s')
    print 'IN SUMMARIZER..'
    posFeatures = []
    negFeatures = []
    posPhrases = {}
    negPhrases = {}
    features = sorted(testEntity['features'], key=lambda p:p['score'])
    for idx, feat in enumerate(features):
        if feat['score'] > 0:
            posFeatures.append(feat['stem'])
            for i, rev in enumerate(feat['pos']):
                tokens = word_tokenizer(rev['text'].lower())
                index = str(idx) + str(i)
                posPhrases[index] = {'tokens': tokens, 'raw': rev['text'].lower(),
                                     'word_frequencies': nltk.FreqDist(tokens).items()}
        elif feat['score'] < 0:
            negFeatures.append(feat['stem'])
            for i, rev in enumerate(feat['neg']):
                tokens = word_tokenizer(rev['text'].lower())
                index = str(idx) + str(i)
                negPhrases[index] = {'tokens': tokens, 'raw': rev['text'].lower(),
                                     'word_frequencies': nltk.FreqDist(tokens).items()}
    #print 'posPhrases LEN: ', len(posPhrases.items())
    posFeatures.reverse()
    #print 'POSFEAT: ', posFeatures
    #print 'negPhrases LEN: ', len(negPhrases.items())
    #print 'NEGFEAT: ', negFeatures
    #for f in features:
        #print f['stem'], ' : ', f['score']
    posSummary = summarizer.summarize(posPhrases, posFeatures).strip(' ')
    negSummary = summarizer.summarize(negPhrases, negFeatures, threshold=2).strip(' ')
    posSummary = posSummary.strip(' ')
    negSummary = negSummary.strip(' ')
    random.seed(time.clock())
    rand = random.randint(0, (len(summarizer.conjWords) - 1))
    conj = summarizer.conjWords[rand]
    negSummary[0].lower()
    if len(posSummary) > 0 and len(negSummary) > 0:
        negSummary = negSummary[0].lower() + negSummary[1:]
        summary = posSummary + ' ' + conj + ', ' + negSummary
    elif len(posSummary) > 0:
        summary = posSummary
    elif len(negSummary) > 0:
        summary = negSummary
    print '\n'
    print 'SUMMARY: '
    print summary

