#import nltk
import cProfile, pstats, StringIO
from textblob import TextBlob
from textblob import Word
from pattern.en import parsetree
from pattern.search import search, taxonomy, Pattern, Constraint
import mongoDataLayer


class Search:

    def __init__(self):

        self.DataLayer = mongoDataLayer.MongoDataLayer()
        self.categories = ['car', 'phone', 'restaurant', 'hotel', 'book', 'consumer', 'movie']
        #use wordnet synset and fuzzy matching and google to come up with these
        self.synCategories = { 'car': ['automotive', 'vehicle'],
          'phone': ['cellphones', 'cell-phones', 'cellular', 'smartphone', 'telephone'],
          'restaurant': ['eatery', 'eating-place', 'bistro'],
          'hotel': ['place to stay', 'inn'],
          'book': ['thing to read', 'story'],
          'movie': ['video','film','thing to watch', 'stuff to watch']}

        self.brands = ['apple', 'microsoft', 'google', 'coca-cola', 'GE', 'HP']
        self.synBrands = {'apple': ['aapl', 'Job\s company'], 'GE': ['general motor'], 'google': ['goog'], 'microsoft': ['msft'], 'facebook': ['fb'], 'hp': ['hewlett-packard']}

        taxonomy.append('category', type='super')
        taxonomy.append('brand', type='super')

        for c in self.categories:
            taxonomy.append(c, type='category')

        for c in self.synCategories.keys():
            for sub in self.synCategories[c]:
                taxonomy.append(sub, type=c)

        for b in self.brands:
            taxonomy.append(b, type='brand')

        for b in self.synBrands:
            for sub in self.synBrands[b]:
                taxonomy.append(sub, type=b)

        self.Patterns = {}
        self.Patterns['basic'] = Pattern.fromstring('SUPER')
        self.Patterns['category'] = Pattern.fromstring('CATEGORY')
        self.Patterns['category_with_feature'] = Pattern.fromstring('CATEGORY with JJ? NP')
        self.Patterns['category_feature'] = Pattern.fromstring('CATEGORY NP')
        #basicQuestionPattern = Pattern.fromstring('what be DT? JJ* SUPER')

    #do more searches refinement(patterns) e.g restaurant in location
    def search(self, q):
        print 'Welcome in SEARCH FUNCTION..'
        pr = cProfile.Profile()
        pr.enable()
        q = q.lower()
        blob = TextBlob(q)
        correctedQuery = blob.correct()
        if correctedQuery.string != q:
            print 'There was mispelling in the query.. Feedback to the user needs to be implemented..'
            print 'DID YOU MEAN: ', correctedQuery

        parsedq = parsetree(correctedQuery.string, lemmata=True)

        if self.Patterns['category_with_feature'].search(parsedq):
            print 'category_with_feature matched'
            nps = []
            match =  self.Patterns['category_with_feature'].match(parsedq)
            for w in match.words:
                if  str(match.constraint(w))== 'Constraint(taxa=[\'category\'])':
                    category =  w.string
                if  str(match.constraint(w))== 'Constraint(chunks=[\'NP\'])':
                    nps.append(w.string)

            feature = ' '.join(x for x in nps)
            #get the right category in case the subCategory was searched
            b = TextBlob(category)
            category = b.words[0].singularize().string
            if category not in self.synCategories.keys():
                for cat in self.synCategories:
                    if category in self.synCategories[cat]:
                        category = cat
                        print 'category changed to ', category
                        break

            #should use lemmas to search NOTE: should use same lettizer in the processor to mk sure we hv consistent shit..
            #TODO turm lemmas into a string
            #lemmas = blob.words.lemmatize()
            category = category.capitalize()
            b = TextBlob(category)
            category = b.words[0].pluralize().string
            print 'Feature is: ' +  feature + ' Category is: ' + category
            pr.disable()
            s = StringIO.StringIO()
            sortby = 'cumulative'
            ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
            ps.print_stats()
            print 'Finished the PATTERN processing, ready to query the DB..'
            print s.getvalue()
            pr2 = cProfile.Profile()
            pr2.enable()
            results = self.DataLayer.searchCategoryFeature(category, feature)
            pr2.disable()
            s2 = StringIO.StringIO()
            ps2 = pstats.Stats(pr2, stream=s2).sort_stats(sortby)
            ps2.print_stats()
            print 'Finished querying the DB..'
            print 'Startin PATTERN processing..'
            print s2.getvalue()
            if len(results) > 1:
                results = results + ', {\'correctedQuery\': \'%s\'}' %correctedQuery
            return results

        elif self.Patterns['category_feature'].search(parsedq):
            print 'category_feature matched'
            nps = []
            match =  self.Patterns['category_feature'].match(parsedq)
            for w in match.words:
                if  str(match.constraint(w))== 'Constraint(taxa=[\'category\'])':
                    category =  w.string
                if  str(match.constraint(w))== 'Constraint(chunks=[\'NP\'])':
                    nps.append(w.string)
            feature = ' '.join(x for x in nps)
            #get the right category in case the subCategory was searched
            b = TextBlob(category)
            category = b.words[0].singularize().string
            if category not in self.synCategories.keys():
                for cat in self.synCategories:
                    if category in self.synCategories[cat]:
                        category = cat
                        break
            #should use lemmas to search NOTE: should use same lettizer in the processor to mk sure we hv consistent shit..
            #TODO turm lemmas into a string
            #lemmas = blob.words.lemmatize()
            category = category.capitalize()
            b = TextBlob(category)
            category = b.words[0].pluralize().string
            print 'Feature is: ' +  feature + ' Category is: ' + category
            results = self.DataLayer.searchCategoryFeature(category, feature)
            if len(results) > 1:
                results = results + ', {\'correctedQuery\': \'%s\'}' %correctedQuery
            return results

        elif self.Patterns['category'].search(parsedq):
            match =  self.Patterns['category'].match(parsedq)
            for w in match.words:
                if  str(match.constraint(w))== 'Constraint(taxa=[\'category\'])':
                    category =  w.string
                    break
            #get the right category in case the subCategory was searched
            b = TextBlob(category)
            category = b.words[0].singularize().string
            if category not in self.synCategories.keys():
                for cat in self.synCategories:
                    if category in self.synCategories[cat]:
                        category = cat
                        print 'category changed to ', category
                        break
            category = category.capitalize()
            b = TextBlob(category)
            category = b.words[0].pluralize().string
            print 'Category is: ', category
            results = self.DataLayer.searchCategory(category)
            if len(results) > 1:
                results = results + ', {\'correctedQuery\': \'%s\'}' %correctedQuery
            return results

        elif self.Patterns['basic'].search(parsedq):
            #Implement basic case
            print 'Basic pattern matched, '
            return ''
        else:
            #needs to implement the lastResort Search..
            return ''
        #return self.MongoDataLayer.testSearch()
