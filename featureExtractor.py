import nltk
from decimal import *
import re
from pattern.web import URL
from pmiPruner import PMIPruner
from classMeronyms import Meronyms
from nltk.corpus import swadesh, stopwords

porter = nltk.PorterStemmer()
getcontext().prec = 3

class FeatureExtractor:

    def __init__(self, className):
        self.MinSup = Decimal(.01)
        self.SubItemsets = []
        self.Sentences = []
        self.StemsDict = {}
        self.StemsAndActualsList = []
        self.PruneCompactSupport = 2
        self.PruneWordDistance = 4
        self.PruneRedundancyPSupport = 3
        self.OneWordFeaturesPSupport = 2
        self.Stuff = {'className': className}
        stopWords = stopwords.words('english')
        self.ExcludeList = swadesh.words('en')
        self.ExcludeList.extend(stopWords)
        self.ExcludeList.extend(['you', 'man', 'fly', 'anything', 'everything', 'something', 'someone', 'more', 'your',
                                 '&', 'any', 'the', 'news', 'a', '|', '-', '_', 'thing', 'place', 'thing', 'state',
                                 'situat', 'occas', 'instanc', 'person', 'peopl', 'someon', 'import', 'signific',
                                 'region', 'world', 'woman', 'locat', 'region', 'date', 'day', 'earth', 'time',
                                 'month', 'sun', 'event', 'brain', 'mind', 'calendar', 'purchas', 'purchase', 'function',
                                 'money', 'review', 'thank', 'use', 'issu', 'issue', 'problem', 'love', 'fine', 'nice'])
        self.ExcludeList = list(set(self.ExcludeList))
        self.ExcludeList = [porter.stem(e.lower()) for e in self.ExcludeList]
        mero = Meronyms(className)
        self.Meronyms = mero.GetMeronyms()
        self.sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
        self.word_tokenizer = nltk.word_tokenize
        self.pos_tagger = nltk.pos_tag

    def Apriori_Gen(self, L):
        c = []
        for itemset in L:
            is_index = L.index(itemset)
            for nxt_index in range(is_index+1, len(L)):
                for f in L[nxt_index]:
                    if f not in L[is_index]:
                        new_itemset = list(L[is_index])
                        new_itemset.append(f)
                        subSetitem = False
                        if len(new_itemset) > 2:
                            if set([new_itemset[0], new_itemset[1]]) in self.SubItemsets:
                                subSetitem = True
                            if set([new_itemset[0], new_itemset[2]]) in self.SubItemsets:
                              subSetitem = True
                            if set([new_itemset[1], new_itemset[2]]) in self.SubItemsets:
                                subSetitem = True
                        if set(new_itemset) not in c and not subSetitem:
                            c.append(set(new_itemset))
        ct = []
        for it in c:
            ct.append(list(it))
        return ct

    def subSupEliminator(self, C, featuresSets, minSup):
        l = []
        for s in C:
            count = 0
            for seti in featuresSets:
                s = set(s)
                if s.issubset(seti):
                    count += 1
                #else:
                    #print 'This not accounted for: ', seti
            if Decimal(count)/Decimal(len(featuresSets)) >= minSup:
              l.append(s)
            else:
              self.SubItemsets.append(s)
        return l

    # TODO re-work the regex part to have a consistant ting
    def Pre_Processor(self, listOfFeatures):
        processedFeatures = []
        for feat in listOfFeatures:
            originalFeat = feat.lower()
            feat = feat.lower()
            parts = re.findall(r'^([a-zA-Z0-9]*?)([^a-zA-Z0-9]+)([a-zA-Z0-9]*)$', feat)
            if len(parts) > 0:
                lparts = list(parts[0])
                for part in lparts:
                    if re.search('[a-zA-Z]+', part):
                        partStem = porter.stem(part)
                        if partStem not in self.ExcludeList:
                            processedFeatures.append(partStem)
                            self.StemsDict[part] = partStem
                            self.StemsAndActualsList.append({'stem': partStem, 'feat': part})
            else:
                stem = porter.stem(feat)
                if stem not in self.ExcludeList:
                    processedFeatures.append(stem)
                    self.StemsDict[originalFeat] = stem
                    self.StemsAndActualsList.append({'stem': stem, 'feat': originalFeat})

        return processedFeatures

    def GetOriginalOrStem(self, stem):
        originals = [key for key in self.StemsDict.keys() if self.StemsDict[key] == stem]
        toUse = [orig for orig in originals if len(orig) <= len(stem)]
        if len(toUse) > 0:
            if stem[len(stem)-1] != toUse[0][len(toUse[0])-1]:
                return stem[0: len(stem)-1]
            else:
                return stem
        else:
            return stem

    def PrunePF(self, features, sentences):
        pruned = []
        if(len(features) > 0):
            if len(features[0]) == 2:
                for f in features:
                    compactness = 0
                    feat1 = self.GetOriginalOrStem(list(f)[0])
                    feat2 = self.GetOriginalOrStem(list(f)[1])
                    for sent in [s for s in sentences if s.find(feat1) != -1 and s.find(feat2) != -1]:
                        i1 = sent.find(feat1) if sent.find(feat1) < sent.find(feat2) else sent.find(feat2)
                        i2 = sent.find(feat1) if sent.find(feat1) > sent.find(feat2) else sent.find(feat2)
                        if sent[i1:i2].count(' ') <= self.PruneWordDistance:
                            compactness += 1
                        if compactness == self.PruneCompactSupport:
                            break
                    if compactness >= self.PruneCompactSupport:
                        pruned.append(f)
            elif len(features[0]) == 3:
              for f in features:
                  compactness = 0
                  feat1 = self.GetOriginalOrStem(list(f)[0])
                  feat2 = self.GetOriginalOrStem(list(f)[1])
                  feat3 = self.GetOriginalOrStem(list(f)[2])
                  for sent in [s for s in sentences if s.find(feat1) != -1 and s.find(feat2) != -1 and s.find(feat3) != -1]:
                      i1 = sent.find(feat1)
                      i2 = sent.find(feat2)
                      i3 = sent.find(feat3)
                      minIndex = min(i1, i2, i3)
                      maxIndex = max(i1, i2, i3)
                      temp = [i for i in [i1, i2, i3] if i > minIndex and i < maxIndex]
                      if len(temp) > 0:
                          midIndex = temp[0]
                          if sent[minIndex:midIndex].count(' ') <= self.PruneWordDistance and sent[midIndex:maxIndex].count(' ') <= self.PruneWordDistance:
                              compactness += 1
                      if compactness == self.PruneCompactSupport:
                          break
                  if compactness >= self.PruneCompactSupport:
                      pruned.append(f)
            else:
                print('The PrunePF does not handle features of length: ', len(features[0]))
        return pruned

    def PruneRedundancy(self, singleFeatures, phraseFeatures, sentences):
        pruned = []
        for f in singleFeatures:
            of = self.GetOriginalOrStem(f)
            superFeats = []
            for pf in phraseFeatures:
              #TODO consider the case of gesture control and gesture-control and gesturecontrol: gesturecontrol is redundant of [gesture, control]
              #TODO continued Pre_Processor turns gesture-control and 'gesture control' into gesturecontrol that way we have same feature for all 3 variations
              #TODO continued if done as described above how do I get phrases where such feature is in? using originalFeat would be the ideal way to do it; check and mk sure that's how it's done..
                if f in pf:
                    superFeats.append(pf)
            if len(superFeats) == 0:
                 pruned.append(f)
            else:
                count = 0
                otherFeats = reduce(lambda x, y: list(x) + list(y), superFeats)
                otherFeats = list(otherFeats)
                otherFeats = list(set(otherFeats))
                otherFeats.remove(f)
                for sent in sentences:
                    if sent.find(of) != -1 and not any(map(lambda x: sent.find(self.GetOriginalOrStem(x)) != -1, otherFeats)):
                        count += 1
                    if count == self.PruneRedundancyPSupport:
                        break
                if count >= self.PruneRedundancyPSupport:
                    pruned.append(f)
        return pruned

    def GetTheMostFrequentFeatureName(self, stem, allStemsAndActualsList):
        hvSameStemFeats = [f['feat'] for f in allStemsAndActualsList if f['stem'] == stem]
        frequ = nltk.FreqDist(hvSameStemFeats)
        return frequ.max()

    def OrderFeaturesWords(self, feature, reviews):
        words = feature.split(' ')
        feat = ''
        '''for p in words:
            feat = feat + self.GetTheMostFrequentFeatureName(p, self.StemsAndActualsList) + ' ' '''
        if len(words) == 2:
            w1 = words[0]
            w2 = words[1]
            indexes1 = [r.index(w1) for r in reviews if w1 in r and w2 in r][0:10]
            indexes2 = [r.index(w2) for r in reviews if w1 in r and w2 in r][0:10]
            if sum(indexes1) < sum(indexes2):
                feat = self.GetTheMostFrequentFeatureName(w1, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w2, self.StemsAndActualsList)
            else:
                feat = self.GetTheMostFrequentFeatureName(w2, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w1, self.StemsAndActualsList)

        elif len(words) == 3:
            w1 = words[0]
            w2 = words[1]
            w3 = words[2]
            indexes1 = [r.index(w1) for r in reviews if w1 in r and w2 in r and w3 in r][0:10]
            indexes2 = [r.index(w2) for r in reviews if w1 in r and w2 in r and w3 in r][0:10]
            indexes3 = [r.index(w3) for r in reviews if w1 in r and w2 in r and w3 in r][0:10]
            if sum(indexes1) < sum(indexes2):
                if sum(indexes2) < sum(indexes3):
                    feat = self.GetTheMostFrequentFeatureName(w1, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w2, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w3, self.StemsAndActualsList)
                else:
                    if sum(indexes3) > sum(indexes1):
                        feat = self.GetTheMostFrequentFeatureName(w1, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w3, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w2, self.StemsAndActualsList)
                    else:
                        feat = self.GetTheMostFrequentFeatureName(w3, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w1, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w2, self.StemsAndActualsList)
            else:
                if sum(indexes1) < sum(indexes3):
                    feat = self.GetTheMostFrequentFeatureName(w2, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w1, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w3, self.StemsAndActualsList)
                else:
                    if sum(indexes3) > sum(indexes2):
                        feat = self.GetTheMostFrequentFeatureName(w2, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w3, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w1, self.StemsAndActualsList)
                    else:
                        feat = self.GetTheMostFrequentFeatureName(w3, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w2, self.StemsAndActualsList) + ' ' + self.GetTheMostFrequentFeatureName(w1, self.StemsAndActualsList)
        return feat.strip()

    def getItemFeatures(self, itemName, reviewsObject):
        print('getting features..')
        reviews = [rev['title'] + '. ' + rev['text'] for rev in reviewsObject]
        features = []
        featuresSets = []
        for r in reviews:
            rns = []
            sents = self.sent_tokenizer.tokenize(r)
            for sent in sents:
                self.Sentences.append(sent.lower())
                tokens = self.word_tokenizer(sent.lower())
                tokens = [t for t in tokens if t.find('http') < 0 and len(t) > 2]
                taggedSent = self.pos_tagger(tokens)
                e = 0
                while e < len(taggedSent):
                    stem = porter.stem(taggedSent[e][0])
                    if taggedSent[e][1].startswith('NN') or stem in self.Meronyms:
                        n = taggedSent[e][0]
                        rns.append(n)
                    e += 1
                processed_rns = self.Pre_Processor(rns)
                featuresSets.append(set(processed_rns))
                features.extend(set(processed_rns))
                rns = []

        print 'FEATURES: ', len(features)
        print 'FEATURESSETS: ', len(featuresSets)
        print 'STEMSDICT: ', len(self.StemsDict)
        print 'STEMSANDACTUALSLIST: ', len(self.StemsAndActualsList)
        if len(features) < 500:
            self.MinSup = Decimal(.03)
            print('MinSup changed to: ', self.MinSup)
        fd = nltk.FreqDist(features)
        self.SubItemsets = [ f for s in featuresSets for f in s if Decimal(fd[f])/Decimal(len(featuresSets)) < self.MinSup]
        self.SubItemsets = list(set(self.SubItemsets))
        tempL1 = [ f for s in featuresSets for f in s if Decimal(fd[f])/Decimal(len(featuresSets)) >= self.MinSup]
        fd2 = nltk.FreqDist(tempL1)
        tempL1 = list(set(tempL1))
        L1 = [[f] for f in tempL1]
        singleFeatures = [ key for key in fd.keys() if fd2[key] >= self.OneWordFeaturesPSupport]
        C2 = self.Apriori_Gen(L1)
        L2 = self.subSupEliminator(C2, featuresSets, self.MinSup)
        C3 = self.Apriori_Gen(L2)
        L3 = self.subSupEliminator(C3, featuresSets, self.MinSup)
        L3 = self.PrunePF(L3, self.Sentences)
        L2 = self.PrunePF(L2, self.Sentences)
        singleFeatures = self.PruneRedundancy(singleFeatures, L2 + L3, self.Sentences)
        meronyms = self.Meronyms
        featuresInMeronyms = [f for f in singleFeatures if f in meronyms]
        featuresNotInMeronyms = set(singleFeatures).difference(featuresInMeronyms)
        pmi = PMIPruner()
        pmiPrunedFeatures = pmi.Prune(itemName, list(featuresNotInMeronyms))

        if 'pmiScrewed' not in pmiPrunedFeatures:
            singleFeatures = featuresInMeronyms + pmiPrunedFeatures
            print('pmi Pruning worked..')
        else:
            print('PMI Pruning didnt work..')

        LStr = []
        for l in L2 + L3:
            l = list(l)
            nn = ' '.join(x for x in l)
            LStr.append(nn)

        stemmedFeatures = []
        stemmedFeatures.extend(singleFeatures)
        stemmedFeatures.extend(LStr)
        allFeatures = {}
        print 'STEMSDICT: ', len(self.StemsDict)
        print 'STEMSANDACTUALSLIST: ', len(self.StemsAndActualsList)
        allStemsAndActualsList = self.StemsAndActualsList

        for s in stemmedFeatures:
            if ' ' not in s:
                allFeatures[s] = self.GetTheMostFrequentFeatureName(s, allStemsAndActualsList)
            else:
                allFeatures[s] = self.OrderFeaturesWords(s, reviews)

        return allFeatures
