var casper = require('casper').create({
	clientScripts: ["/Users/pmahoro/Projects/Other/reviewize/libs/jquery.js"]
});
var fs = require("fs");

var ProductsLinks = [], rootUrl = 'http://www.decide.com';
var Category = casper.cli.get(0) || casper.cli.get('cat');
if(!Category){
	this.echo('Please provide a category in the form: $casperjs file.js cat');
	this.exit();
}

var StartPageUrl = 'http://decide.com/search/'+Category;

if(!fs.isDirectory(Category))
	fs.makeDirectory(Category);
if(fs.workingDirectory != Category)
	fs.changeWorkingDirectory(Category);


function getProductsUrl(){

	var links = [];

	$('.product_tile').each(function(){

		links.push($(this).attr('data-url'));

	});

	// USE the following two lines when it CRASHES before finishing the category.. Otherwise comment it out
	var lastDownloadeItemUrl ='/p/laptops-samsung-ativ-book-4-ativ-book-117297447';
	links = links.slice(links.indexOf(lastDownloadeItemUrl), links.length);

	return links;
}

function getFirstPageInfo(){

	var p = {};
	p['usreviews'] = [];
	p['expreviews'] = [];

	//TODO move the part below to getAllUsersReviews..

	var uRevs = $('ul.users.review_list li.row'),
		eRevs = $('ul.expert.review_list li.row'),
		rev = {},
		t, i;

	if(uRevs){
		for(i=0; i < uRevs.length; i++){

			//TODO Add review SOURCE and LINK
			t = $(uRevs[i]);
			rev['rating'] = $(t).find('.review_data.span2 div.rating_container span:nth(0)').text();
			rev['author'] = $(t).find('.review_data.span2 div.author_container span:nth(0)').text();
			rev['date'] = $(t).find('.review_data.span2 div.date').text();
			rev['description'] = $(t).find('.review_text.span10 p.text_blob span:nth(0)').text();
			rev['from'] = $(t).find('.review_text.span10 p.text_blob span.from a').text();
			rev['link'] = $(t).find('.review_text.span10 p.text_blob span.from a').attr('href');

			p['usreviews'].push(rev);
		}
	}
	if(eRevs){
		for(i=0; i < eRevs.length; i++){
			t = $(eRevs[i]);
			rev['rating'] = $(t).find('.review_data.span2 div.rating_container span:nth(0)').text();
			rev['author'] = $(t).find('.review_data.span2 div.author_container span:nth(0)').text();
			rev['date'] = $(t).find('.review_data.span2 div.date').text();
			rev['description'] = $(t).find('.review_text.span10 .review_bit').text();
			rev['from'] = rev['author'];
			rev['link'] = $(t).find('.review_text.span10 p a').attr('href');

			p['expreviews'].push(rev);
		}
	}

	p['sentiments'] = $('.sentiments.row');
	p['specs'] = $('section.product_specs');
	p['stores'] = $('#online_prices_page_contents .table-striped');

	return p;

}

function navProducts(){


	casper.each(ProductsLinks, function(self, link) {

		var product = {};

		this.thenOpen(rootUrl + link, function() {

			var name = this.getTitle();
			if(name.indexOf('/') > -1)
				name = name.substring(0, name.indexOf('/'));

			name = name.replace(/\-/g, '');
			name = name.replace(/\(/g, '');
			name = name.replace(/\)/g, '');

			product['name'] = name;

			this.echo('The name is: ' + product['name']);

			var temp = this.evaluate(getFirstPageInfo);

			product['usersReviews'] = temp['usreviews'];
			product['expertsReviews'] = temp['expreviews'];
			product['sentiments']= temp['sentiments'];
			product['specs'] = temp['specs'];
			product['stores'] = temp['stores'];

			if(product['usersReviews'])
				this.echo('usersReviews length: ' + product['usersReviews'].length);
			if(product['expertsReviews'])
				this.echo('expertsReviews length: ' + product['expertsReviews'].length)

			var imgUrl = this.evaluate(function(){

				return $('div.product_image_container.span4 img').attr('src');

			});

			this.captureSelector(name + '.jpg',"img[src=\"" + imgUrl + "\"]");

		});

		this.then(function(){

			var allUsersReviewsLinks = this.evaluate(function(){

				var links = [];
				var reviewsNum = parseInt($('span[itemprop="reviewCount"]').text());
				var pageNum = reviewsNum / 5;
				var modelLink = $($('.users div.pagination ul li a')[0]).attr('href');
				var linkPart1 = modelLink.substring(0, modelLink.indexOf('?')-1);
				var linkPart2 = modelLink.substring(modelLink.indexOf('?'), modelLink.length);

				for(var i=2; i<= pageNum; i++){
					links.push(linkPart1 + i + linkPart2);
				}

				/*$('.users div.pagination ul li a').each(function(){
				 links.push($(this).attr('href'))
				 });*/

				return links;
			});

			if(allUsersReviewsLinks)
				this.echo('Users reviews links count: ' + allUsersReviewsLinks.length);

			casper.each(allUsersReviewsLinks, function(self, link){

				this.thenOpen(rootUrl + link, function(){

					var more;

					this.then(function(){

						more = this.evaluate(function(){

							var revs = [],
							    uRevs = $('.page_content li.row'),
								rev = {}, t;
							if(uRevs){
								for(var i=0; i < uRevs.length; i++){
									t = $(uRevs[i]);
									rev['rating'] = $(t).find('.review_data.span2 div.rating_container span:nth(0)').text();
									rev['author'] = $(t).find('.review_data.span2 div.author_container span:nth(0)').text();
									rev['date'] = $(t).find('.review_data.span2 div.date').text();
									rev['description'] = $(t).find('.review_text.span10 p.text_blob span:nth(0)').text();
									rev['from'] = $(t).find('.review_text.span10 p.text_blob span.from a').text();
									rev['link'] = $(t).find('.review_text.span10 p.text_blob span.from a').attr('href');
									revs.push(rev);
								}
							}
							return revs;

						});

					});

					this.then(function(){

						for(var i=0; i < more.length; i++){
							product['usersReviews'].push(more[i]);
						}
					});

				});
				this.then(function(){
					if(product['usersReviews'])
						this.echo('Users Reviews After appending: ' + product['usersReviews'].length);
				});

			});

		});

		this.thenOpen(rootUrl + link,function(){

			var allExpertsReviewsLinks = this.evaluate(function(){
				var links = [];
				$('.expert div.pagination ul li a').each(function(){
					links.push($(this).attr('href'))
				});
				return links;
			});

			if(allExpertsReviewsLinks)
				this.echo('Experts reviews links count; ' + allExpertsReviewsLinks.length);

			casper.each(allExpertsReviewsLinks, function(self, link){

				this.thenOpen(rootUrl + link, function(){

					var more = this.evaluate(function(){

						var revs = [],
							rev = {}, t;
						var eRevs = $('.page_content li.row');
						if(eRevs){
							for(var i=0; i < eRevs.length; i++){
								t = $(eRevs[i]);
								rev['rating'] = $(t).find('.review_data.span2 div.rating_container span:nth(0)').text();
								rev['author'] = $(t).find('.review_data.span2 div.author_container span:nth(0)').text();
								rev['date'] = $(t).find('.review_data.span2 div.date').text();
								rev['description'] = $(t).find('.review_text.span10 .review_bit').text();
								rev['from'] = rev['author'];
								rev['link'] = $(t).find('.review_text.span10 p a').attr('href');

								revs.push(rev);
							}
						}
						return revs;
					});

					for(var i=0; i < more.length; i++){
						product['expertsReviews'].push(more[i]);
					}

				});

				this.then(function(){
					if(product['expertsReviews'])
						this.echo('Expert reviews after appending: ' + product['expertsReviews'].length);
				});

			});

		});

		this.then(function(){
			var productStr = JSON.stringify(product);
			fs.write( product['name'] + ".txt", productStr, 'w');
		});
	})	;


};

function scroll(casper){
	casper.echo('Trying to scroll....');
	casper.wait(2000,
		casper.evaluate(function(){
			window.document.body.scrollTop = document.body.scrollHeight;
		}));
}

function scroller(casper){

	casper.waitForText('All results loaded',
		function(){
			casper.echo('Scrollin Done');
		}, function(){
			scroll(casper);
			scroller(casper);
		});
}

casper.start(StartPageUrl);

casper.then(function(){
	scroller(this);
});

casper.then(function(){
	ProductsLinks = casper.evaluate(getProductsUrl);
	if(ProductsLinks)
		casper.echo('Count: ' + ProductsLinks.length);
});

casper.then(navProducts);

casper.run();
