var jqueryPath = "/Users/floturatimana/Projects/Reviewize/jquery.js";
var fs = require('fs');

var casper = require('casper').create({
                                      clientScripts: [jqueryPath]
                                      });


function getLinks() {
    //var links = document.querySelectorAll('span.rvwCnt').length;
    var links = document.querySelectorAll('span.crAvgStars a:nth-child(2)');
    return Array.prototype.map.call(links, function(e) {
                                    return e.getAttribute('href');
                                    });
}

function getReviews(){

    var reviews = [];
    
    $('#productReviews div').each(function(){
        
       try{
       var t = this;
       var index = 0;
                                  
       if(t.innerText.indexOf('found the following review helpful') == -1)
          index = 0;
       else
          index = 1
                                  
       var r = {}
       r.starRating = t.getElementsByTagName('div')[index].childNodes[1].innerText;
       r.starRating = parseInt(r.starRating.substring(0,1));
       r.title = t.getElementsByTagName('div')[index].childNodes[3].childNodes[0].innerText;
       r.date = t.getElementsByTagName('div')[index].childNodes[3].childNodes[2].innerText;
                                  
       //var date = t.find('nobr')[0].innerText
       //var starRating = t[0].children[1].getElementsByClassName('swSprite')[0].innerText
                                  
       r.content = '';
                                  
       for(i in t.childNodes){
          if(t.childNodes[i].nodeName == '#text')
            r.content += t.childNodes[i].nodeValue
          }
       r.content = r.content.replace(/^[\s]+|[\s]+$/g, '')
       
       if(reviews.indexOf(r) == -1)
       reviews.push(r);
     }
     catch(e){}
    });
    
    return reviews;
}

function getNextLink(){

    var index = -1, count = '';
    var p =document.querySelector('.paging');
    var chil = p.children;
    for(i in chil ){
        //console.log(p.children[i])
        if(index == 1){
            count = i;
            index=-1;
            break;
        }
        try{
            if(chil[i].getAttribute('class') == 'on'){
                console.log(i);
                index = 1;}
        }
        
        catch(e){}
    }
    try{
        return chil[count].getAttribute('href')}
    catch(e){
        return -1;
    }
    
    
}

casper.start('http://amazon.com');

casper.then(function(){
            
            this.echo('On amazon already: ' + this.getTitle());
            //this.fill('input[id=twotabsearchtextbox]', {'field-keywords':'shoe'}, true);
            
     });

//TODO follow pagination

var reviews = [];

function nextStep(casper, nextLink){
    casper.echo('Passed nextLink is: ' + nextLink);
    casper.thenOpen(nextLink, function(){
                    
                  casper.echo('getting next reviews from: ' + nextLink)
                  casper.then(function(){
                        reviews = reviews.concat(casper.evaluate(getReviews));
                         })
                  casper.then(function(){
                        nextLink = casper.evaluate(getNextLink);      
                        })
                  casper.then(function(){
                        casper.echo('Next linked changed to: ' + nextLink);
                        casper.echo('Reviews length: ' + reviews.length)
                        if(nextLink != -1)
                              nextStep(casper, nextLink);
                          });

          });
}

var randomURL = 'http://www.amazon.com/s/ref=sr_nr_seeall_4?rh=k%3Adiaper%2Ci%3Ababy-products&keywords=diaper&ie=UTF8&qid=1380958247'
var keywordURL = 'http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=mower'

casper.thenOpen(keywordURL, function(){
       
        /*this.wait(5000, function(){
                          fs.write("test.html", this.getHTML(), "w");
                          });*/
                
        casper.then(function(){
             var testReviewsFile = "testReviews.txt";
             try{
                var reviewsStr = fs.read(testReviewsFile);
                reviews = JSON.parse(reviewsStr);
                this.echo('Got saved reviews: ' + reviews.length);
              }
              catch(e){
                    this.echo('Could not open: ', testReviewsFile)
                    reviews = []
               }
      
          });
        var links = this.evaluate(getLinks);
        this.echo('Reviews numbers: ' + this.getHTML('span.crAvgStars a:nth-child(2)'));
        
        this.each(links, function(self, link){
                  var nextLink = 'google.com';
                      this.thenOpen(link, function(){
                                    
                             this.echo('Title: ' + this.getTitle());
                             this.echo('Link: ' + link);
                             
                             this.then(function(){
                                    reviews = reviews.concat(this.evaluate(getReviews));
                                 })
                             this.then(function(){
                                 nextLink = this.evaluate(getNextLink);;             
                                     })
                             this.wait(2000, function(){
                                       
                                 this.then(function(){
                                         if(nextLink)
                                         nextStep(this, nextLink);
                                    });
                              });
                             
                         });
             });
  });

casper.then(function(){
      var reviewsStr = JSON.stringify(reviews);
      fs.write("testReviews.txt", reviewsStr, "w");
      this.echo('Saved the ' + reviews.length + ' review.');
      });

casper.run();



