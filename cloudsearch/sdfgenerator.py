import uuid
import json
from pprint import pprint
from os import listdir, getcwd, path, makedirs
# from os import path, makedirs # isfile, join, splitext
import sys
import logging
import argparse

SDF_VERSION = 1
SDF_LANGUAGE = "en"
OUTPUT_DIR =  "/Users/pmahoro/Projects/Other/reviewize_processor/cloudsearch/samples/output/"
VERBOSE = False

class FieldValidator(object):
    @staticmethod
    def get(field, data):
        value = None
        if field in data:
            value = data[field]
            
        status = True
        if value is None:
            status = False
        if isinstance(value, (list, tuple)) and len(value) == 0:
            status = False
        
        if VERBOSE == True and status == False:
            logging.info('field %s is empty', field)
        return value    

class SDFGenerator(object):
    """
    This class is used to generator a SDF(Search Document Format) files that we use with AWS cloud search
    It expects to be passed a raw file (.txt, .json) as input and will look, generate an SDF document.
    """
    
    def __init__(self, file=None, dest=None, categoryName=None, actiontype="add"):
        self.inputFile = file
        self.type = actiontype
        self.outputDir = dest
        self.output = {}
        self.data = None
        self.isParsed = False
        self.category = categoryName
        self.generateDoc()
        
    def generateDoc(self):
        readSuccess = self.addFieldsData()
        if readSuccess == False:
            logging.error('Skipping file: %s, because of several missing required fields', self.inputFile)
            return
            
        self.addDocumentMeta()
        self.writeToFile(self.output)

    def parseFile(self, aFile):
        if aFile is None:
            #raise an error
            logging.error("your input file cannot be empty. Returning")
            return
        
        with open(aFile) as data_file:
            try: 
                self.data = json.load(data_file)
            except ValueError:
                logging.error('Could not open the file properly: %s', aFile)
            finally:
                self.encoding = data_file.encoding
                if self.data is None:
                    self.data = []
               
        
    def writeToFile(self, data):
        outFileName = self.getOuputDir() + self.inputFile.split('/')[-1]
        with open(outFileName, 'w+') as outFile:
            json.dump(self.output, outFile)
        
    
    def getVersion(self):
        return SDF_VERSION
    
    def getOuputDir(self):
        if self.outputDir != None:
            return self.outputDir
        return self.OUTPUT_DIR
        
    def getLang(self):
        return SDF_LANGUAGE
    
    def getUniqueId(self):
        cls = self.__class__
        return self.__class__.UUIDGenerator()
        
    def getContentEncoding(self):
        return "ISO-8859-1"
        
    def getContentType(self):
        return "text/plain"
                
    def addDocumentMeta(self):
        out = self.output
        out['type'] = self.type
        out['id'] = self.getUniqueId()
        out['lang'] = self.getLang()
        out['version'] = self.getVersion()
        out['content_type'] = self.getContentType()
        out['content_encoding'] = self.getContentEncoding()
        
    def retrieveFeatures(self):
        """
        Currently, we have features as a list of successive repetitive stuff in this format:
            {"featureName":"Display Size","featureValue":"11.6 in ."},{"featureName":"Width","featureValue":"11.22 in."}
        in this function, we want to get rid of 'featureName' and 'featureValue' keywords and
        just return key-value pairs such as: 
            {"Display Size": "11.6 in ."}
        """
        
        d = []
        key = None
        value = None
        try:
            if not 'specs' in self.data:
                return d
            
            specs = self.data['specs']
                
            features = FieldValidator.get('features',  specs)
            # If no features, just return.
            if not features:
                return d
            
            for feature in features:
                key = feature['featureName']
                value = feature['featureValue']
                if key != None:
                    d.append({key:value})
            
        except Exception as inst:
            logging.error('Error Occurred: %s', type(inst))     # the exception instance
            raise
        finally:
            d = json.dumps(d)
        return d 
    
    def addFieldsData(self):
        
        if self.data is None and self.isParsed != True :
            logging.info('parsing file: %s', self.inputFile)
            self.parseFile(self.inputFile)
            self.isParsed = True    
        
        # Start add fields data
        fields = {}
        success = True
        try:
            if ('name' in self.data) == False:
                logging.error('Missing required field: name')
                success = False

            fields['name'] = FieldValidator.get('name', self.data);
            fields['experts_reviews'] = FieldValidator.get("expertsReviews", self.data)
            fields['sentiments'] = FieldValidator.get('sentiments', self.data)
            fields['users_reviews'] = FieldValidator.get('usersReviews', self.data)
            fields['short_description'] = FieldValidator.get('shortDescription', self.data)
            fields['category'] = self.category
            
            # Since the AWs cloud search expects only string, we have to stringify arrays.
            fields['users_reviews'] = json.dumps(fields['users_reviews'])
            fields['experts_reviews'] = json.dumps(fields['experts_reviews'])
            fields['sentiments'] =  json.dumps( fields['sentiments'] )
            
            # fields['decideScore'] = self.data['decideScore']

            if ('specs' in self.data):
                specs = self.data['specs']
                fields['description'] = FieldValidator.get('description', specs)
                fields['features'] = self.retrieveFeatures()

            self.output['fields'] = fields
        except Exception as e:
            logging.error('Error occurred: %s', type(e))
            raise
            success = False
        return success
    
    @classmethod
    def UUIDGenerator(cls):
        return  str(uuid.uuid1()).replace('-', '_')
        
    # @staticmethod
    # def generate(file):
    #     generator = SDFGenerator(file=file)

def listFilesInDir(root = None):
    acceptedFiles = ['.txt', '.json']
    
    if root is None:
        root = getcwd()
    
    files = [ f for f in listdir(root) if path.isfile(path.join(root,f))]
    r = []
    for f in files:
        file, extension = path.splitext(f)
        if extension in acceptedFiles:
            fname = root + f
            r.append(fname)
    
    return r
    
def ensure_dir(f):
    d = path.dirname(f)
    if not path.exists(d):
        makedirs(d)


def processFiles(lst, inputDir, categoryName=None):
    if lst is None:
        logging.error('No files to handle')
        return
    
    try:
        outputDir = inputDir + "processed/"
        ensure_dir(outputDir)
        logging.info('Created output dir: %s', outputDir)
    except Exception as e:
        logging.error('Failed to create destination directory. Error: %s', type(e))
     
    for l in lst:
        logging.info('Processing: %s', l)
        SDFGenerator(file = l, dest = outputDir, categoryName = categoryName)
        
def main(argv):
    # Logging setup. 
    # TODO: Make this go to a file.
    
    if argv['verbose'] == True:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    else:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.WARNING)
    
    try:
        myPath = argv['path']
        categoryName = argv['category']
    except Exception as e:
        logging.error('Error occurred: %s', type(e))
        logging.info('Make sure you provide a input dir (The dir contains files you want to create a batch for).')
        
    finally:
        logging.info("target path: %s", myPath)
        logging.warning('Category: %s', categoryName)
        inputFiles = listFilesInDir(myPath)
        processFiles(inputFiles, myPath, categoryName)
    
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='''\
    Converts json files into Amazon SDF files. It expects to be given a path
    that contains input files and the category name. 
    e.g. python sdfgenerator.py -p  /path/to/my/input/directory -c category1''')
                                                  
    parser.add_argument('-p','--path', help='Input directory', required=True)
    parser.add_argument('-c','--category', help='Category Name', required=True)
    parser.add_argument('-v', '--verbose', action='store_true')
    
    myargs = vars(parser.parse_args())
    print myargs
    main(myargs)
    
#Need unit tests for
# making sure the version number is int
# UUIDs contains only valid characters...