import urllib
import urllib2, httplib


def main():
    
    url = 'http://www.google.com/'
    values = {}
    
    data = urllib.urlencode(values)
    
    try:
        conn = httplib.HTTPConnection("www.python.org")
        conn.request("GET", "/index.html")
        r1 = conn.getresponse()
        print r1.status, r1.reason

        data1 = r1.read()
        conn.request("GET", "/parrot.spam")
        r2 = conn.getresponse()
        print r2.status, r2.reason

        data2 = r2.read()
        conn.close()
    except urllib2.HTTPError as e:
        print 'The server couldn\'t fulfill the request.'
        print 'Error code: ', e.code
    except urllib2.URLError as e:
        print 'We failed to reach a server.'
        print 'Reason: ', e.reason
    
if __name__ == "__main__":
    main()
