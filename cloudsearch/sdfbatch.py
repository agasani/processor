from os import listdir, getcwd, makedirs
from os import path # isfile, join, splitext
from datetime import date
import sys
import json
import logging
import argparse

BATCH_DEST = ""
# BATCH_LENGTH 

class BatchSDF:
    """
    This class creates a batched Amazon's cloudsearch SDF file.
    """
    
    def listFilesInDir(self, root = None):
        acceptedFiles = ['.txt', '.json']
    
        if root is None:
            root = getcwd()
    
        files = [ f for f in listdir(root) if path.isfile(path.join(root,f))]
        r = []
        for f in files:
            extension = path.splitext(f)[1]
            if extension in acceptedFiles:
                fname = root + f
                r.append(fname)
        return r
        
    def batchFileName(self, dirName):
        today = date.today()
        today = str(today)
        dirName = dirName+"/batch/"
        
        self.ensure_dir(dirName)
        return dirName + 'batch_' + today + '.txt'
    
    def ensure_dir(self, f):
        d = path.dirname(f)
        if not path.exists(d):
            makedirs(d) 

    def batchFileInDir(self, directory):
        if directory is None:
            logging.error("Please provide a dir")
            return
        
        if not path.isdir(directory) :
            logging.error("please provide a dir. "+ dir +" is not a directory")
            return
        
        data = []                                
        fileLst = self.listFilesInDir(directory)
        bName = self.batchFileName(directory)
        
        with open(bName, 'w+') as outFile:
            for f in fileLst:
                try:
                    with open(f) as inputFile:
                        logging.info("start reading file: %s", f)
                        data.append( json.load(inputFile) )
                        logging.info("end reading file: %s", f)
                except Exception as inst:
                    logging.error('Error happened reading file %s', f)
                    logging.debug('Error type: %s, args: %s', inst.type, inst.arg)
                    
            json.dump(data, outFile)        
        

def main(argv):
    
    # Logging setup. 
    # TODO: Make this go to a file.
    if argv['verbose'] == True:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    else:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.WARNING)
    
    try: 
        myPath = argv['path']
        logging.info("target dir: %s", myPath)
        batch = BatchSDF()
        batch.batchFileInDir(myPath)
    except Exception:
        logging.error('Provide a directory argument (The dir contains files you want to create a batch for).')
    
    
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='''\
    Batches several files into one large batch file. \
    Right now, it only accepts a path to the directory that contains the files to batch. 
    e.g. python sdfbatch.py -p  /path/to/my/input/directory''')
                                                  
    parser.add_argument('-p','--path', help='Input directory', required=True)
    parser.add_argument('-v', '--verbose', action='store_true')
    
    myargs = vars(parser.parse_args())
    logging.info('args: %s', myargs)
    main(myargs)
              