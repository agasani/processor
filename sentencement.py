import nltk
import re
import sentimentTokenizer as Tokenizer
from decimal import *
from math import sqrt
#import json
#import random
#import pickle
#from nltk.corpus import movie_reviews

'''from pymongo import MongoClient
    
client = MongoClient();
db = client.ReviewizeDB;
testData = db.testData;'''

getcontext().prec = 3
#Sentence_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
Tokenizer = Tokenizer.Tokenizer(preserve_case=False)
Tokenize = Tokenizer.tokenize
Tag = nltk.pos_tag
porter = nltk.PorterStemmer()
StopWords = ['.', ':', ';', '!', '?', '...', '..']
SpecialStopWords = ['but', 'however', 'though', 'tho']
LinkingVerbs = ['be', 'look', 'get']
TAG_RE = re.compile(r'<[^>]+>')

posFile = 'Resources/positive-words.txt'
negFile = 'Resources/negative-words.txt'
testDataFile = 'Resources/testReviews.txt'
matchStemRXPattern = '[a-zA-Z]{0,4}'
try:
    with open(posFile, 'r') as pf:
        posWords = pf.read()
        posWords = posWords.split('\n')
except:
    print 'Could not open: ', posFile
    posWords= []

try:
    with open(negFile, 'r') as nf:
        negWords = nf.read()
        negWords = negWords.split('\n')
except:
    print 'Could not open: ', negFile
    negWords= []


def getWordPolarity(word):
    
    sentiment = 0
    
    if word in negWords:
        sentiment = -1
    else:
        sentiment = sentiment
    
    if word in posWords:
        sentiment = 1
    else:
        sentiment = sentiment
    
    return sentiment

def findPattern1(words,tagList, currentIndex):
    #this finds the 'couldnt be happier' kinda pattern
    nxtIndex = currentIndex + 1
    nxtWord = words[nxtIndex]
    isThePattern = False
    if nxtWord in LinkingVerbs:
        for i in range(1, 5):
            if isThePattern is True:
                break
            xWord = words[nxtIndex + i]
            theTag = tagList[xWord]
            if theTag == 'JJR':
                isThePattern = True
            elif theTag == 'RBR':
                oWord = words[nxtIndex + i + 1]
                oTag = tagList[oWord]
                if oTag.startswith('JJ'):
                    isThePattern = True

    return isThePattern

def remove_tags(text):
    return TAG_RE.sub(' ', text)

def getSentencePolarity(sentence):
    score = 0
    words = Tokenize(sentence)
    tags = Tag(words)
    tagList = {}
    for tag in tags:
        tagList[tag[0]] = tag[1]
    
    negate = False
    
    for w in words:
        if re.search(r"(?:^(?:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint)$)|n't", w):
            try:
                currentIndex = words.index(w)
                if w in ['couldnt', 'couldn\'t']:
                    isThePattern = findPattern1(words, tagList, words.index(w))
                    if isThePattern is True:
                        negate = False
                        continue
                    else:
                        negate = True
                        continue
                elif w == 'not':
                    previousIndex = currentIndex - 1
                    previousWord = words[previousIndex]
                    if previousWord == 'could':
                        isThePattern = findPattern1(words, tagList, words.index(w))
                        if isThePattern is True:
                            negate = False
                            continue
                        else:
                            negate = True
                            continue
                    else:
                        negate = True
                        continue
                else:
                    negate = True
                    continue

            except:
                negate = True
                continue

        if re.match(r"^[.:;!?]$", w):
            negate = False
            continue

        if negate == False:
            if tagList[w].startswith('JJ') or tagList[w].startswith('VB') or tagList[w].startswith('RB') or tagList[w].startswith('NN'):
              try:  
                if w.encode('ascii','ignore') in posWords:
                  score = score + 1
                elif w.encode('ascii','ignore') in negWords:
                  score = score - 1
              except:
                print 'The word causing issues is: ', w , ' and its type is: ', type(w)
                score = score

        elif negate == True:
            if tagList[w].startswith('JJ') or tagList[w].startswith('VB') or tagList[w].startswith('RB') or tagList[w].startswith('NN'):
              try:
                if w.encode('ascii','ignore') in posWords:
                  score = score - 1
                elif w.encode('ascii','ignore') in negWords:
                  score = score + 1
              except:
                print 'The word causing issues is: ', w , ' and its type is: ', type(w)
                score = score    
    
    return score

def getCount(tokens, lemma):
  count = 0
  for w in tokens:
    if re.search(r'\b' + lemma + matchStemRXPattern + r'\b', w):
      count = count + 1
  return count

def getIndex(tokens, lemma):
  index = -1
  for w in tokens:
    if re.search(r'\b' + lemma + matchStemRXPattern + r'\b', w):
      index = tokens.index(w)
      break
  return index

def getShortSentence(sentence, feat):
  tokens = Tokenize(sentence)
  index = getIndex(tokens, feat)
  sentenceTokens = []
  for w in tokens[index: index + 16]:
    if w not in SpecialStopWords and w is not None:
      sentenceTokens.append(w)
    if w in StopWords or w in SpecialStopWords:
      break

  temp = []
  lIndex = index - 16
  if lIndex < 0:
    lIndex = 0
  tempTokens = tokens[lIndex: index]
  tempTokens.reverse()

  for w in tempTokens:
    if w not in StopWords:
      temp.append(w)
    if w in StopWords or w in SpecialStopWords:
      break

  temp.reverse()
  temp.extend(sentenceTokens)
  sentenceTokens = temp

  return ' '.join(sentenceTokens)#.replace('none', '')

def getSentence(review, featz, meronyms=[]):
    #The below is not necessary since I fixed the buy where I was getting partial reviews text with None in it
    #review = remove_tags(review)
    word_tokenizer = nltk.word_tokenize
    pos_tagger = nltk.pos_tag

    sents = nltk.sent_tokenize(review)
    if len(featz) == 1:
        for sent in sents:
            sent = sent.replace('-', ' ')
            if re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', sent):
                tokens = word_tokenizer(sent)
                tags = pos_tagger(tokens)
                for tag in tags:
                    stem = porter.stem(tag[0])
                    if (tag[1].startswith('NN') or stem in meronyms) and re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', tag[0]):
                        #print 'TAG MATCHED: sent should be added: ', tag[0], '  :  ', sent
                        if len(sent) > 215:
                            sent = getShortSentence(sent, featz[0])
                        return sent
                    elif tag[0] == featz[0]:
                        print 'TAG AINT MATCH: sent shouldnt be added: ', tag[0], '   ', sent

    elif len(featz) == 2:
        for sent in sents:
            sent = sent.replace('-', ' ')
            f1Search = re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', sent)
            f2Search = re.search(r'\b' + featz[1] + matchStemRXPattern + r'\b', sent)
            if f1Search and f2Search:

                #TOTHINKBOUT the following is to make sure the features' word-proximity is below 4
                #I might want to ignore the word-proximity, CONS: I'd get unrelated word-features PROS: I realized if
                #a 2 or 3-gram appears often enough, it is meaningful even though them together don't make any sense
                #an example I noticed was 'phone version'
                #TOTHINKBOUT shoudl I do like the above, case for single feature, where I make sure features are 'NN'?

                wordTokens = Tokenize(sent)
                i1 = getIndex(wordTokens, featz[0])
                i2 = getIndex(wordTokens, featz[1])
                if abs(i2 - i1) < 4:
                    return sent
                else:
                    appearanceCount1 = getCount(wordTokens, featz[0])
                    appearanceCount2 = getCount(wordTokens, featz[1])
                    length = len(wordTokens)
                    for c1 in range(0, appearanceCount1):
                        i11 = getIndex(wordTokens[i1:length], featz[0])
                        if i1 < (length-1):
                            i1 = getIndex(wordTokens[(i1+1):length], featz[0])
                        i2Copy = i2
                        for c2 in range(0, appearanceCount2):
                            i22 = getIndex(wordTokens[i2Copy:length], featz[1])
                            if abs(i22 - i11) < 4:
                                return sent
                            if i2Copy < (length-1):
                                i2Copy = getIndex(wordTokens[(i2Copy+1):length], featz[1])
            return None

    elif len(featz) == 3:
        for sent in sents:
            sent = sent.replace('-', ' ')
            f1Search = re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', sent)
            f2Search = re.search(r'\b' + featz[1] + matchStemRXPattern + r'\b', sent)
            f3Search = re.search(r'\b' + featz[2] + matchStemRXPattern + r'\b', sent)
            if f1Search and f2Search and f3Search:
                wordTokens = Tokenize(sent)
                i1 = getIndex(wordTokens, featz[0])
                i2 = getIndex(wordTokens, featz[1])
                i3 = getIndex(wordTokens, featz[2])
                mx = max(i1, i2, i3)
                mn = min(i1, i2, i3)
                if abs(mx - mn) < 8:
                    return sent
                else:
                    appearanceCount1 = getCount(wordTokens, featz[0])
                    appearanceCount2 = getCount(wordTokens, featz[1])
                    appearanceCount3 = getCount(wordTokens, featz[2])
                    length = len(wordTokens)
                    for c1 in range(0, appearanceCount1):
                        i11 = getIndex(wordTokens[i1:length], featz[0])
                        if i1 < (length-1):
                            i1 = getIndex(wordTokens[(i1+1):length], featz[0])
                        i2Copy = i2
                        for c2 in range(0, appearanceCount2):
                            i22 = getIndex(wordTokens[i2Copy:length], featz[1])
                            if i2Copy < (length-1):
                                i2Copy = getIndex(wordTokens[(i2Copy+1):length], featz[1])
                            i3Copy = i3
                            for c3 in range(0, appearanceCount3):
                                i33 = getIndex(wordTokens[i3Copy:length], featz[2])
                                mx2 = max(i11, i22, i33)
                                mn2 = min(i11, i22, i33)
                                if abs(mx2 - mn2) < 8:
                                    return sent
                                if i3Copy < (length-1):
                                    i3Copy = getIndex(wordTokens[(i3Copy+1):length], featz[2])
            return None
    else:
        return None

#review = testData.find({'starRating': 5}).limit(1)[0]['content']

def getFeatureRanking(pos, neg):
    '''total = pos + neg
    if pos == neg:
        return 0
    elif pos == 0 and neg > 0:
        return -5
    elif neg == 0 and pos > 0:
        return 5
    elif pos > neg:
        return round((float(pos * 5)/total))
    elif neg > pos:
        return - round((float(neg * 5)/total))'''
    total = pos + neg
    if total == 0:
        return 0
    z = 1.64485
    if pos > neg:
        p = float(pos)/total
        confidence = (p+z*z/(2*total)-z*sqrt((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)
    else:
        p = float(neg)/total
        confidence = -(p+z*z/(2*total)-z*sqrt((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)

    return round(confidence * 3, 2)

def getSentiments(reviewsObject, features, meronyms):

  print 'in sentiments..'

  featuresObj = {}

  for feat in features:
    featuresObj[feat] = {}
    featuresObj[feat]['pos'] = []
    featuresObj[feat]['neg'] = []
    featuresObj[feat]['neut'] = []
    featuresObj[feat]['score'] = 0
    featz = feat.split(' ')

    if len(featz) == 1:
        reviewsWitFeature = [rev for rev in reviewsObject if re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', rev['title'] + '. ' + rev['text'])]
    elif len(featz) == 2:
        reviewsWitFeature = [rev for rev in reviewsObject if re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', rev['title'] + '. ' + rev['text'])
                             and re.search(r'\b' + featz[1] + matchStemRXPattern + r'\b', rev['title'] + '. ' + rev['text'])]
    elif len(featz) == 3:
        reviewsWitFeature = [rev for rev in reviewsObject if re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', rev['title'] + '. ' + rev['text'])
                             and re.search(r'\b' + featz[1] + matchStemRXPattern + r'\b', rev['title'] + '. ' + rev['text'])
                             and re.search(r'\b' + featz[2] + matchStemRXPattern + r'\b', rev['title'] + '. ' + rev['text'])]

    for review in reviewsWitFeature:
      sente = getSentence(review['title'] + '. ' + review['text'], featz, meronyms)
      if sente is not None:
          polarity = getSentencePolarity(sente)
          if 'source' in review:
            sentObject = {'text': sente, 'author': review['reviewer'], 'url': review['link'], 'source': review['source']}
          else:
            sentObject = {'text': sente, 'author': review['reviewer'], 'url': 'http://amazon.com' + review['link'], 'source': 'amazon.com'}
          if polarity > 0:
            featuresObj[feat]['pos'].append(sentObject)
          elif polarity < 0:
            featuresObj[feat]['neg'].append(sentObject)
          elif len(sente) > 6:
            featuresObj[feat]['neut'].append(sentObject)

    pos = len(featuresObj[feat]['pos'])
    neg = len(featuresObj[feat]['neg'])
    score = getFeatureRanking(pos, neg)
    featuresObj[feat]['score'] = score

  return featuresObj

'''

def getSentiments_Old(reviews, features):

  #reviews = ['I bought them and the shoes are very good; they make good work boots but they are bad church shoes.', 'I hate those red shoes.', 'I bought these work boots.']

  #features = ['work boots', 'shoes']
  print 'in sentiments..'
  print 'Features are: ', features

  featuresObj = {}
  for feat in features:
    featuresObj[feat] = {}
    featuresObj[feat]['pos'] = []
    featuresObj[feat]['neg'] = []
    featuresObj[feat]['neut'] = []
    featuresObj[feat]['score'] = 0
    featz = feat.split(' ')
    if len(featz) == 1:
        reviewsWitFeature = [rev for rev in reviews if re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', rev)]
    elif len(featz) == 2:
        reviewsWitFeature = [rev for rev in reviews if re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', rev) and re.search(r'\b' + featz[1] + matchStemRXPattern + r'\b', rev)]
    elif len(featz) == 3:
        print 'THE FEAT IS: ', featz[0]
        reviewsWitFeature = [rev for rev in reviews if re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', rev) and re.search(r'\b' + featz[1] + matchStemRXPattern + r'\b', rev) and re.search(r'\b' + featz[2] + matchStemRXPattern + r'\b', rev)]
        print 'REVS WIT ', featz[0], reviewsWitFeature
    for review in reviewsWitFeature:
      review = review.lower()
      tokens = Tokenize(review)
      featureSentence = []
       
      if len(featz) == 1:
        index = getIndex(tokens, featz[0])
        #print 'The index for: ', featz[0], ' is ', index
        #why the fuck do I get the sentence this way? The best way would be to chunk the review into sentence and look for the feature in those sentences
        #Figured why: I word-tokenize so that I can have word approximity for multi-words features
        featureSentence = getSentence(index, tokens)

        #print 'The sentence is: ', ' '.join(featureSentence)
            
      elif len(featz) == 2:
        allCounts = getCount(tokens, featz[0])
        temp = tokens
        for i in range(0,allCounts):
          index = getIndex(temp, featz[0])
          index2 = getIndex(temp, featz[1])
          #print 'feature is: ', featz
          #print 'Tokens are: ', temp
          #print 'Index1 is: ', index
          #print 'index2 is: ', index2
          if abs(index2 - index) < 4:
              featureSentence = getSentence(index2, tokens)
              break
          else:
              temp = temp[index + 1 : len(temp)]

      elif len(featz) == 3:
        allCounts = getCount(tokens, featz[0])
        temp = tokens
        for i in range(0,allCounts):
          index = getIndex(temp, featz[0])
          index2 = getIndex(temp, featz[1])
          index3 = getIndex(temp, featz[2])
          if (abs(index2 - index) + abs(index3 - index)) < 8:
              featureSentence = getSentence(index2, tokens)
              break
          else:
              temp = temp[index + 1 : len(temp)]

      sente = ' '.join(featureSentence)
     
      polarity = getSentencePolarity(sente)

      if polarity > 0:
        featuresObj[feat]['pos'].append(sente)
      elif polarity < 0:
        featuresObj[feat]['neg'].append(sente)
      elif len(sente) > 6:
        featuresObj[feat]['neut'].append(sente)

    #if (len(featuresObj[feat]['neg']) + len(featuresObj[feat]['pos'])) != 0:
      #negScore = (len(featuresObj[feat]['neg']) * 100)/(len(featuresObj[feat]['neg']) + len(featuresObj[feat]['pos']))
      #posScore = (len(featuresObj[feat]['pos']) * 100)/(len(featuresObj[feat]['neg']) + len(featuresObj[feat]['pos']))
    #else:
      #negScore, posScore = 0, 0
    #score = posScore - negScore
    #featuresObj[feat]['score'] = float(Decimal(score)/Decimal(20))

    pos = len(featuresObj[feat]['pos'])
    neg = len(featuresObj[feat]['neg'])
    score = getFeatureRanking(pos, neg)
    featuresObj[feat]['score'] = score

  return featuresObj
'''
#sentence = raw_input('Enter your review: ')

#print 'The sentence score is: ', getSentencePolarity(sentence)




