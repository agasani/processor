__author__ = 'agasani'

import hmac
import base64
import hashlib
import urllib2
import urllib
import datetime
import xml.etree.ElementTree as ET

class PriceGrabber:

    #TODO make it work for other searchIndex like Books.. now the indexing onlyworks for Electronics; see if using tags could solve the index ting..
    def __init__(self):
        self.key = 'AKIAIX5VSU7YG5HH7JLQ'
        self.secre = 'J39h6uaAxdQ2BOToQV+0IiHg95HZ5MelCj4xRnW0'
        self.associateTag = 'ibihuhacom-20'
        self.version = '2015-03-10'
        self.topStrWithNoBreaks = 'http://ecs.amazonaws.com/onca/xml?AWSAccessKeyId=' + self.key + '&AssociateTag=' + self.associateTag
        self.topStrWithBreaks = 'GET\n'\
                        'ecs.amazonaws.com\n'\
                        '/onca/xml\n'\
                        'AWSAccessKeyId=' + self.key + '&AssociateTag=' + self.associateTag


    def encodeString(self, string):
        theString = string.replace(' ', '%20')
        return theString

    def sign(self, theString):
        #print 'THE STRING TO SIGN IS: ', theString
        dig = hmac.new(self.secre, msg=theString, digestmod=hashlib.sha256).digest()
        signature = base64.b64encode(dig).decode()
        return signature

    def getTimestamp(self):
        temp = datetime.datetime.utcnow().isoformat()
        timestamp = str(temp)
        return timestamp

    def searchKeywordsQuery(self, keywords, searchIndex):
        timestamp = self.getTimestamp()
        encodeHeywords = self.encodeString(keywords)
        #searchQuery = urllib.urlencode({'Keywords': keywords,'Operation': 'ItemSearch', 'SearchIndex': searchIndex, 'Service': 'AWSECommerceService', 'Timestamp': timestamp, 'Version': self.version})
        searchQuery = 'Keywords=' + encodeHeywords + '&Operation=ItemSearch' + '&ResponseGroup=OfferSummary' + '&SearchIndex=' + searchIndex +  '&Service=AWSECommerceService' + '&' + urllib.urlencode({'Timestamp': timestamp}) + '&Version=' + self.version
        return searchQuery

    def searchOfferQuery(self, name, searchIndex):
        timestamp = self.getTimestamp()
        encodeKeywords = self.encodeString(name)
        #searchQuery = urllib.urlencode({'Keywords': keywords,'Operation': 'ItemSearch', 'SearchIndex': searchIndex, 'Service': 'AWSECommerceService', 'Timestamp': timestamp, 'Version': self.version})
        searchQuery = 'Keywords=' + encodeKeywords + '&Operation=ItemSearch' + '&ResponseGroup=OfferSummary' + '&SearchIndex=' + searchIndex +  '&Service=AWSECommerceService' + '&' + urllib.urlencode({'Timestamp': timestamp}) + '&Version=' + self.version
        return searchQuery

    def getItemByASINQuery(self, asin):
        timestamp = self.getTimestamp()
        searchQuery = 'ItemId=' + asin + '&Operation=ItemLookup' + '&ResponseGroup=OfferFull' + '&Service=AWSECommerceService' + '&' + urllib.urlencode({'Timestamp': timestamp}) + '&Version=' + self.version
        return searchQuery

    def getItemAttrQuery(self, asin):
        timestamp = self.getTimestamp()
        searchQuery = 'ItemId=' + asin + '&Operation=ItemLookup' + '&ResponseGroup=ItemAttributes' + '&Service=AWSECommerceService' + '&' + urllib.urlencode({'Timestamp': timestamp}) + '&Version=' + self.version
        return searchQuery

    def getOffers(self, name, searchIndex):
        try:
            searchPart = self.searchOfferQuery(name, searchIndex)
            toSignStr = self.topStrWithBreaks + '&' + searchPart
            signature = self.sign(toSignStr)
            url = self.topStrWithNoBreaks + '&' + searchPart + '&' + urllib.urlencode({'Signature': signature})
            print 'SEARCH URL: ', url
            f = urllib2.urlopen(url)
            xml = f.read()
            return xml
        except:
            return None

    def getItemXml(self, asin):
        try:
            searchPart = self.getItemByASINQuery(asin)
            toSignStr = self.topStrWithBreaks + '&' + searchPart
            signature = self.sign(toSignStr)
            url = self.topStrWithNoBreaks + '&' + searchPart + '&' + urllib.urlencode({'Signature': signature})
            print 'Item/ASIN URL is: ', url
            f = urllib2.urlopen(url)
            xml = f.read()
            return xml
        except Exception as e:
            raise
            return None

    def getItemAttrXml(self, asin):
        try:
            searchPart = self.getItemAttrQuery(asin)
            toSignStr = self.topStrWithBreaks + '&' + searchPart
            signature = self.sign(toSignStr)
            url = self.topStrWithNoBreaks + '&' + searchPart + '&' + urllib.urlencode({'Signature': signature})
            print 'ATTR URL is: ', url
            f = urllib2.urlopen(url)
            xml = f.read()
            return xml
        except Exception as e:
            raise
            return None

    def getUrlForElectronics(self, xml):
        try:
            root = ET.fromstring(xml)
            item = root[1].findall('.')[0][1]
            #print 'item TAG: ', item.tag
            url = item[3][2].text
            return url
        except Exception as e:
            print 'Failed to get Link For Electronics from the item xml'
            raise
            return None

    def getUrlForBooks(self):
        try:
            pass
        except:
            pass

    def getFullOfferUrlByASIN(self, asin):
        try:
            theXml = self.getItemXml(asin)
            url = self.getUrlForElectronics(theXml)
            return url
        except Exception as e:
            raise
            return None

    def getItemAttrByASIN(self, asin):
        try:
            xml = self.getItemAttrXml(asin)
            root = ET.fromstring(xml)
            wildAmazonTagPadding = '{http://webservices.amazon.com/AWSECommerceService/2011-08-01}'
            items = root.find(wildAmazonTagPadding + 'Items')
            request, item, isValid, detailsLink, itemLinks, itemAttr, specsLink, reviewsLink, offersLink, brand, amznCat, \
                EAN, listPrice, manufacturer, model, typeName, productGroup, size, UPC, title =\
                None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None
            if items is not None:
                request = items.find(wildAmazonTagPadding + 'Request')
                item = items.find(wildAmazonTagPadding + 'Item')
            if request is not None:
                isValid = request.find(wildAmazonTagPadding + 'IsValid').text
            if item is not None:
                detailsLink = item.find(wildAmazonTagPadding + 'DetailPageURL').text
                itemLinks = item.find(wildAmazonTagPadding + 'ItemLinks')
                itemAttr = item.find(wildAmazonTagPadding + 'ItemAttributes')
            if itemLinks is not None:
                for link in itemLinks.findall(wildAmazonTagPadding + 'ItemLink'):
                    desc = link.find(wildAmazonTagPadding + 'Description').text
                    if 'Technical' in desc:
                        specsLink = link.find(wildAmazonTagPadding + 'URL').text
                    if 'Review' in desc:
                        reviewsLink = link.find(wildAmazonTagPadding + 'URL').text
                    if 'Offer' in desc:
                        offersLink = link.find(wildAmazonTagPadding + 'URL').text
            if itemAttr is not None:
                brandEl = itemAttr.find(wildAmazonTagPadding + 'Brand')
                if brandEl is not None:
                    brand = brandEl.text
                amznCatEl = itemAttr.find(wildAmazonTagPadding + 'Binding')
                if amznCatEl is not None:
                    amznCat = amznCatEl.text
                eanEl = itemAttr.find(wildAmazonTagPadding + 'EAN')
                if eanEl is not None:
                    EAN = eanEl.text
                listPriceEl = itemAttr.find(wildAmazonTagPadding + 'ListPrice')
                if listPriceEl is not None:
                    listPrice = float(listPriceEl.find(wildAmazonTagPadding + 'Amount').text)/100
                manufEl = itemAttr.find(wildAmazonTagPadding + 'Manufacturer')
                if manufEl is not None:
                    manufacturer = manufEl.text
                modelEl = itemAttr.find(wildAmazonTagPadding + 'Model')
                if modelEl is not None:
                    model = modelEl.text
                typeEl = itemAttr.find(wildAmazonTagPadding + 'ProductTypeName')
                if typeEl is not None:
                    typeName = typeEl.text
                groupEl = itemAttr.find(wildAmazonTagPadding + 'ProductGroup')
                if groupEl is not None:
                    productGroup = groupEl.text
                sizeEl = itemAttr.find(wildAmazonTagPadding + 'Size')
                if sizeEl is not None:
                    size = sizeEl.text
                upcEl = itemAttr.find(wildAmazonTagPadding + 'UPC')
                if upcEl is not None:
                    UPC = upcEl.text
                titleEl = itemAttr.find(wildAmazonTagPadding + 'Title')
                if titleEl is not None:
                    title = titleEl.text
                dimensEl = itemAttr.find(wildAmazonTagPadding + 'ItemDimensions')
                dimensions = {}
                if dimensEl is not None:
                    for child in dimensEl:
                        if 'Height' in child.tag:
                            dimensions['Height'] = child.text
                        if 'Length' in child.tag:
                            dimensions['Length'] = child.text
                        if 'Weight' in child.tag:
                            dimensions['Weight'] = child.text
                        if 'Width' in child.tag:
                            dimensions['Width'] = child.text
                features = []
                for feat in itemAttr.findall(wildAmazonTagPadding + 'Feature'):
                    features.append(feat.text)

            return {'title': title,'isValid': isValid, 'detailsLink': detailsLink,
                        'specsLink': specsLink, 'reviewsLink': reviewsLink, 'offersLink': offersLink, 'brand': brand, 'EAN': EAN,
                        'amznCat': amznCat, 'listPrice': listPrice, 'manufacturer': manufacturer,'model': model, 'typeName': typeName,
                         'productGroup': productGroup, 'size': size, 'UPC': UPC, 'features': features, 'dimensions': dimensions}

        except Exception as e:
            raise
            return None

    def processOfferXml2(self, xml):
        try:
            root = ET.fromstring(xml)
            #print 'ROOT TAG: ', root.tag, ' ATTRIB: ', root.attrib
            wildAmazonTagPadding = '{http://webservices.amazon.com/AWSECommerceService/2011-08-01}'
            items = root.find(wildAmazonTagPadding + 'Items')
            request, firstItem, isValid, asin, offerSummary, loNuEl, loUsedEl, totalNew, totalUsed, parentASIN, lowestUsedPrice, lowestNewPrice\
                = None, None, None, None, None, None, None, None, None, None, None, None
            if items is not None:
                request = items.find(wildAmazonTagPadding + 'Request')
                firstItem = items.find(wildAmazonTagPadding + 'Item')
            if request is not None:
                isValid = request.find(wildAmazonTagPadding + 'IsValid').text
            if firstItem is not None:
                asin = firstItem.find(wildAmazonTagPadding + 'ASIN').text
                parentASINEl = firstItem.find(wildAmazonTagPadding + 'ParentASIN')
                if parentASINEl is not None:
                    parentASIN = parentASINEl.text
                offerSummary = firstItem.find(wildAmazonTagPadding + 'OfferSummary')
            if offerSummary is not None:
                loNuEl = offerSummary.find(wildAmazonTagPadding + 'LowestNewPrice')
                loUsedEl = offerSummary.find(wildAmazonTagPadding + 'LowestUsedPrice')
                totalNew = offerSummary.find(wildAmazonTagPadding + 'TotalNew').text
                totalUsed = offerSummary.find(wildAmazonTagPadding + 'TotalUsed').text
            if loNuEl is not None:
                lowestNewPrice = loNuEl.find(wildAmazonTagPadding + 'Amount').text
            if loUsedEl is not None:
                lowestUsedPrice = loUsedEl.find(wildAmazonTagPadding + 'Amount').text

            newP = 0
            usedP = 0
            refP = 0
            if lowestNewPrice is not None:
                newP = float(lowestNewPrice)/100
            if lowestUsedPrice is not None:
                usedP = float(lowestUsedPrice)/100
            #if refurburshedPrice is not None:
                #refP = float(refurburshedPrice)

            #for items in root.iter('*'):
                #print 'INLOOP', 'TAG: ', items.tag, 'ATTR: ', items.attrib
            return {'isValid': isValid, 'ASIN': asin, 'parentASIN': parentASIN, 'newPrice': newP, 'totalNew': totalNew,
                    'usedPrice': usedP, 'totalUsed': totalUsed}
        except Exception as ex:
            raise

    def processOfferXml(self, xml):
        try:
            root = ET.fromstring(xml)
            t = root[1]
            print 'ROOT', t
            isValid = t.findall('.')[0][0][0].text
            #totalResults = t.findall('.')[0][1].text
            #searchResultUrl = t.findall('.')[0][3].text
            firstItem = t.findall('.')[0][4]
            firstItemAsin = firstItem[0].text
            firstItemOfferUrl = self.getFullOfferUrlByASIN(firstItemAsin)
            newPrice = firstItem[2][0][0].text
            usedPrice = firstItem[2][1][0].text
            refurburshedPrice = firstItem[2][2][0].text
            lowestPrice = 0
            newP = 0
            usedP = 0
            refP = 0
            if newPrice is not None:
                newP = float(newPrice)
            if usedPrice is not None:
                usedP = float(usedPrice)
            if refurburshedPrice is not None:
                refP = float(refurburshedPrice)
            lowestPrice = min(newP, usedP, refP)/100
            return {'isValid': isValid, 'ASIN': firstItemAsin, 'price': lowestPrice, 'offerUrl': firstItemOfferUrl }
        except Exception as ex:
            raise
            return None

    def test(self):
        #searchPart = 'Keywords=harry%20potter&Operation=ItemSearch&SearchIndex=Books&Service=AWSECommerceService&' + urllib.urlencode({'Timestamp': timestamp}) + '&Version=2011-08-01'
        searchPart = self.searchKeywordsQuery('harry potter', 'Books')
        print 'SEARCH PART: ', searchPart
        toSignTest = self.topStrWithBreaks + '&' + searchPart
        signature = self.sign(toSignTest)
        url = self.topStrWithNoBreaks + '&' + searchPart + '&' + urllib.urlencode({'Signature': signature})
        print 'THE REST URL IS: ', url
        f = urllib2.urlopen(url)
        print f.read()

    def getPricing(self, name, seachIndex):
        xml = self.getOffers(name, seachIndex)
        pricingObject = self.processOfferXml2(xml)
        #firstItemOfferUrl = self.getFullOfferUrlByASIN(pricingObject['ASIN'])
        #pricingObject['offerUrl'] = firstItemOfferUrl
        attrs = self.getItemAttrByASIN(pricingObject['ASIN'])
        #TODO get images
        if pricingObject['isValid'] == 'True' and attrs['isValid']:
            allAttrs = pricingObject.copy()
            allAttrs.update(attrs)
            return allAttrs
        elif pricingObject['isValid'] == 'True':
            return pricingObject
        elif attrs['isValid'] == 'True':
            return attrs

if __name__ == '__main__':
    #TEST
    p = PriceGrabber()
    #p.test()
    pricingObject = p.getPricing('Fifty shades of grey', 'Books')
    print pricingObject
    #req = urllib2.Request(url='http://www.google.com', data='test')
    #url = 'http://www.google.com'
    #f = urllib2.urlopen(url)
    #print f.read()
