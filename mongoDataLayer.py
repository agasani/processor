import os, os.path
import json
import sys

from pymongo import MongoClient

client = MongoClient()
db = client.criticjam

class MongoDataLayer:

  def __init__(self, collectionName):
      self.collection = db[collectionName]
      pass
      
  def getReviewsFromFolder(self, path):
    
    for root, _, files in os.walk(path):
        for f in files:
            fullPath = os.path.join(root, f)
            fileName, fileExt = os.path.splitext(fullPath)
            fileName = os.path.basename(fileName)
            if fileExt == '.txt':
                f = open(fullPath, 'r')
                content = f.read()
                obj = json.loads(content)
                phone = {}
                phone['name'] = fileName

                try:
                     phone['usersReviews'] = obj['usersReviews']
                except KeyError:
                     print fileName, ' RAISED A KEYERROR EXCEPTION: NO usersReviews Key'

                try:
                    phone['expertsReviews'] = obj['expertsReviews']
                except KeyError:
                    print fileName, ' RAISED A KEYERROR EXCEPTION: NO expertsReviews Key'

                for attr in phone:
                    print attr
                #try:

                            #collection.insert(phone)

                #except:
                    #print 'could not parse: ', fileName
                    #print sys.exc_info()[0]
                print fullPath
                f.close()

                '''
                try:
                    print fullPath
                    with open(fullPath, 'r') as f:
                            content = f.read()
                        #try:
                            obj = json.loads(content)
                            phone = {}
                            phone['name'] = fileName
                            
                            try:
                                phone['usersReviews'] = obj['usersReviews']
                            except KeyError:
                                print fileName, ' RAISED A KEYERROR EXCEPTION: NO usersReviews Key'
                            
                            try:
                                phone['expertsReviews'] = obj['expertsReviews']
                            except KeyError:
                                print fileName, ' RAISED A KEYERROR EXCEPTION: NO expertsReviews Key'
                            i = 0
                            for attr in phone and i in range(0, 10):
                                print attr
                                i = i + 1
                            #self.collection.insert(phone)
                        
                        #except:
                            #print 'could not parse: ', fileName
                            #print sys.exc_info()[0]
                except:
                    print 'Could not open: ', f
'''

  def getReviewsFromFile(self, fileName):
      reviews = []
      try:
          with open(fileName, 'r') as f:
              rawReviews = f.read()
              reviews = rawReviews.split('<FIN>')
      except:
          print 'Could not open ', fileName
          reviews = []
      return reviews

  def getTestReviews(self):
      '''
      fileName = 'Resources/sumsangG4Reviews.txt'
      try:
          with open(fileName, 'r') as f:
              rawReviews = f.read()
              reviews = rawReviews.split('<FIN>')
      except:
          print 'Could not open ', fileName
          reviews = []
      '''
      reviews = []
      r = self.collection.find_one( {'usersReviews' : {'$exists': True}, '$where':'this.usersReviews.length > 600'} )
      #p = r.next()
      #print r
      revs = r['usersReviews']
      for i in revs:
           reviews.append(i['description'])
      return reviews

  def getProductForTest(self, name=None):
      #product = self.collection.find_one({'$and': [{'features': {'$exists': True}}, {'reviews': {'$exists': True}, '$where':'this.reviews.length < 29'}]})
      product = self.collection.find_one({'name': name})
      return product


  def getQuickTestReviews(self):
      reviews = ['Its camera is good shit device in a camera. I really love the camera and its control.', 'The image quality is good.', 'it\'s huge it cannot fit in a coach bag is really the straw and I love going screen.',
    'image qualities is great.', 'image is good and quality is fine.', 'the size is big and I love looking at the screen.',
    'the screen size is ridiculous.', 'the screen is entertaining but comes at a cost in size.', 'I love this phone. And I like the panorama game apps screen',
    'its cameras takes awesome pictures and viewing them on the big size screens make me good', 'game apps make it an entertainment center with image quality',
    'the gesture control is good on its screen.', 'I got this phone as a gift and I love the gesture-control and I love touching the screen.']
      return reviews

  def getAllDocs(self):
      docs = self.collection.find()
      return docs

  def getProductWithNoFeatures(self):
      prods = self.collection.find({'$and': [{'features': {'$exists': False}},
                                             {'reviews': {'$exists': True}, '$where':'this.reviews.length > 29'}]},
                                   no_cursor_timeout=True).batch_size(40)
      return prods

  def getProductsNotInDynamo(self):
      prods = self.collection.find({'$and': [{'saved': {'$exists': False}},
                                             {'features': {'$exists': True}, '$where':'this.reviews.length > 29'}]})
      return prods

  def add(self, obj):
    self.collection.insert(obj)
    
  def update(self, name, superCategory, category, synCategory, features, score):
    self.collection.update({'name': name}, {'$set': {'SuperCategory': superCategory, 'Category': category, 'SynCategory':synCategory, 'Features': features, 'Score': score}})

  def updateFeaturesOld(self, name, score, features, numReviews):
      self.collection.update({'name': name}, {'$set': {'Features': features, 'Score': score, 'NumReviews': numReviews}})

  def updateFeatures(self, name, features):
      self.collection.update({'name': name}, {'$set': {'features': features}})

  def updateSummaryDescription(self, name, desc):
      self.collection.update({'name': name}, {'$set': {'desc': desc}})

  def addFieldsToDoc(self, name, data):
      self.collection.update({'name': name}, {'$set': {data}})

  def updateScore(self, name, score):
      self.collection.update({'name': name}, {'$set': {'score': score}})

  def updateSaved(self, name):
      self.collection.update({'name': name}, {'$set': {'saved': True}})

  def updateDescription(self, name, description):
      self.collection.update({'name': name}, {'$set': {'description': description}})

  def getOneRandomDoc(self):
    return self.collection.find_one()
    
  def findByName(self, name):
    self.collection.find_one({'name': name})

#print 'Lets get fuckin started..'

#myClass = MongoDataLayer()
#path = '../Data/All Reviews/Phones_Products_with_all_Reviews'
#myClass.getReviewsFromFolder(path)

#print 'Done...'
