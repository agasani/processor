import nltk
import re
import sentimentTokenizer as Tokenizer
from decimal import *
from math import sqrt
from pymongo import MongoClient
import pickle

client = MongoClient()
db = client.ReviewizeDB
phones = db.Phones
phone = phones.find_one({'NumReviews': { '$gt': 1000}})
reviews = phone['usersReviews']
posReviews = [rev for rev in reviews if rev['rating'] == '5']
negReviews = [rev for rev in reviews if rev['rating'] == '1']
CountProgress = 0

Sentence_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
Tokenizer = Tokenizer.Tokenizer(preserve_case=False)
matchStemRXPattern = '[a-zA-Z]{0,4}'
TAG_RE = re.compile(r'<[^>]+>')

getcontext().prec = 3

StopWords = ['.', ':', ';', '!', '?', '...', '..']
SpecialStopWords = ['but', 'however', 'though', 'tho']
LinkingVerbs = ['be', 'look', 'get']

posFile = 'Resources/positive-words.txt'
negFile = 'Resources/negative-words.txt'
testDataFile = 'Resources/testReviews.txt'

try:
    with open(posFile, 'r') as pf:
        posWords = pf.read()
        posWords = posWords.split('\n')
except:
    print 'Could not open: ', posFile
    posWords= []

try:
    with open(negFile, 'r') as nf:
        negWords = nf.read()
        negWords = negWords.split('\n')
except:
    print 'Could not open: ', negFile
    negWords= []

def getEntityRanking(pos, neg):
    total = pos + neg
    if total == 0:
        return 0
    z = 1.64485
    p = float(pos)/total
    #confidence = sqrt(p+z*z/(2*total)-z*((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)
    confidence = (p+z*z/(2*total)-z*sqrt((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)
    print 'Before the normalization ting, confidence is: ', confidence
    return round(confidence * 10 / 2, 1)

def getFeatureRanking(pos, neg):
    total = pos + neg
    if pos == neg:
        return 0
    elif pos == 0 and neg > 0:
        return -5
    elif neg == 0 and pos > 0:
        return 5
    elif pos > neg:
        return round((float(pos * 5)/total))
    elif neg > pos:
        return - round((float(neg * 5)/total))

def findPattern1(words,tagList, currentIndex):
    #this finds the 'couldnt be happier' kinda pattern
    nxtIndex = currentIndex + 1
    nxtWord = words[nxtIndex]
    isThePattern = False
    if nxtWord in LinkingVerbs:
        for i in range(1, 5):
            if isThePattern is True:
                break
            xWord = words[nxtIndex + i]
            theTag = tagList[xWord]
            if theTag == 'JJR':
                isThePattern = True
            elif theTag == 'RBR':
                oWord = words[nxtIndex + i + 1]
                oTag = tagList[oWord]
                if oTag.startswith('JJ'):
                    isThePattern = True

    return isThePattern


def getSentencePolarity(sentence):
    score = 0
    words = Tokenizer.tokenize(sentence)
    tags = nltk.pos_tag(words)
    tagList = {}
    for tag in tags:
        tagList[tag[0]] = tag[1]

    negate = False

    for w in words:
        if re.search(r"(?:^(?:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint)$)|n't", w):
            try:
                currentIndex = words.index(w)
                if w in ['couldnt', 'couldn\'t']:
                    isThePattern = findPattern1(words, tagList, words.index(w))
                    if isThePattern is True:
                        negate = False
                        continue
                    else:
                        negate = True
                        continue
                elif w == 'not':
                    previousIndex = currentIndex - 1
                    previousWord = words[previousIndex]
                    if previousWord == 'could':
                        isThePattern = findPattern1(words, tagList, words.index(w))
                        if isThePattern is True:
                            negate = False
                            continue
                        else:
                            negate = True
                            continue
                    else:
                        negate = True
                        continue
                else:
                    negate = True
                    continue

            except:
                negate = True
                continue

        if re.match(r"^[.:;!?]$", w):
            negate = False
            continue

        if negate == False:
            if tagList[w].startswith('JJ') or tagList[w].startswith('VB') or tagList[w].startswith('RB') or tagList[w].startswith('NN'):
              try:
                if w.encode('ascii','ignore') in posWords:
                  score = score + 1
                elif w.encode('ascii','ignore') in negWords:
                  score = score - 1
              except:
                print 'The word causing issues is: ', w , ' and its type is: ', type(w)
                score = score

        elif negate == True:
            if tagList[w].startswith('JJ') or tagList[w].startswith('VB') or tagList[w].startswith('RB') or tagList[w].startswith('NN'):
              try:
                if w.encode('ascii','ignore') in posWords:
                  score = score - 1
                elif w.encode('ascii','ignore') in negWords:
                  score = score + 1
              except:
                print 'The word causing issues is: ', w , ' and its type is: ', type(w)
                score = score

    return score

def applyNegation(review):
    reviewWords = Tokenizer.tokenize(review)
    tags = nltk.pos_tag(reviewWords)
    tagList = {}
    for tag in tags:
        tagList[tag[0]] = tag[1]
    processed = []
    negate = False

    for w in reviewWords:
        if re.search(r"(?:^(?:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint)$)|n't", w):
            negate = True
            continue
        if re.match(r"^[.:;!?]$", w):
            negate = False
            continue
        if negate == False:
            if tagList[w].startswith('JJ') or tagList[w].startswith('VB'):# or tagList[w].startswith('RB'):
                processed.append(w)
        elif negate == True:
            if tagList[w].startswith('JJ') or tagList[w].startswith('VB'):# or tagList[w].startswith('RB'):
                processed.append(w + '_NEG')

    return processed

def getReviewPolarity(review):
    sentences = Sentence_tokenizer.tokenize(review)
    revScore = 0
    #print sentences
    for sent in sentences:
        revScore = revScore + getSentencePolarity(sent)

    if revScore > 0:
        return 1
    else:
        return -1

def getReviewFeatures(review):
    features = {}
    sentences = Sentence_tokenizer.tokenize(review)
    #print 'Sentences len: ', len(sentences)
    psp, nsp = 0.0, 0.0 #psp: Positive Sentence Percentage nps: Negative Sentence Percentage
    for sent in sentences:
        sentPolarity = getReviewPolarity(sent)
        if sentPolarity == 1:
            psp = psp + 1
        elif sentPolarity == -1:
            nsp = nsp + 1

    features['psp'] = int((Decimal(psp)/Decimal(len(sentences))) * 100)
    features['nsp'] = int((Decimal(nsp)/Decimal(len(sentences))) * 100)
    #features['polarity'] = getReviewPolarity(review)
    adjNverbsWitNeg = applyNegation(review)

    #for word in adjNverbsWitNeg:
    #   features['contains(%s)' % word] = (word in adjNverbsWitNeg)

    for word in adjNverbsWitNeg:
        if '_NEG' in word:
            temp = word[0:word.index('_NEG')]
            if temp.encode('ascii', 'ignore') in posWords:
                features['(%s).isPos' % temp] = False
            elif temp.encode('ascii', 'ignore') in negWords:
                features['(%s).isNeg' % temp] = False
        else:
            if word.encode('ascii', 'ignore') in posWords:
                features['(%s).isPos' % word] = True
            elif word.encode('ascii', 'ignore') in negWords:
                features['(%s).isNeg' % word] = True

    '''global CountProgress
    CountProgress = CountProgress + 1
    print 'Progress: ', CountProgress'''
    return features

def testSingleReview(review):
    polarity = getReviewPolarity(review)
    if polarity > 0:
        print 'The review is POSITIVE'
    else:
        print 'The review is NEGATIVE'

def testSimpleClassifier(posReviews, negReviews):
    print 'pos LEN: ', len(posReviews)
    print 'neg LEN: ', len(negReviews)

    numFalseNeg = 0
    numFalsePos = 0

    for rev in posReviews:
        polarity = getReviewPolarity(rev['description'])
        if polarity < 0:
            numFalseNeg = numFalseNeg + 1
            print 'FALSENEG:  ', rev['description']

    print 'numFalseNeg: ', numFalseNeg
    print 'POS Precision is: ', (len(posReviews) - numFalseNeg)*100/len(posReviews)

    for rev in negReviews:
        polarity = getReviewPolarity(rev['description'])
        if polarity > 0:
            numFalsePos = numFalsePos + 1
    print 'numFalsePos: ', numFalsePos
    print 'NEG precision is: ', (len(negReviews) - numFalsePos)*100/len(negReviews)

def testTrainedClassifier(posReviews, negReviews):
    print 'pos LEN: ', len(posReviews)
    print 'neg LEN: ', len(negReviews)
    numFalseNeg = 0
    numFalsePos = 0
    f = open('Resources/NBClassifierV1.pickle')
    NBClassifier = pickle.load(f)
    f.close()
    f = open('Resources/MEClassifierV1.pickle')
    #MEClassifier = pickle.load(f)
    f.close()

    for rev in posReviews:
        features = getReviewFeatures(rev['description'])
        polarity = NBClassifier.classify(features)
        #polarity = MEClassifier.classify(features)
        if polarity == 'neg':
            numFalseNeg = numFalseNeg + 1

    print 'numFalseNeg: ', numFalseNeg
    print 'POS Precision is: ', (len(posReviews) - numFalseNeg)*100/len(posReviews)

    for rev in negReviews:
        features = getReviewFeatures(rev['description'])
        polarity = NBClassifier.classify(features)
        #polarity = MEClassifier.classify(features)
        if polarity == 'pos':
            numFalsePos = numFalsePos + 1

    print 'numFalsePos: ', numFalsePos
    print 'NEG precision is: ', (len(negReviews) - numFalsePos)*100/len(negReviews)

def testNewRankAlg(posNum, negNum):
    total = posNum + negNum
    if total == 0:
        return 0
    z = 1.64485
    if posNum > negNum:
        p = float(posNum)/total
        confidence = (p+z*z/(2*total)-z*sqrt((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)
    else:
        p = float(negNum)/total
        confidence = -(p+z*z/(2*total)-z*sqrt((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)

    print 'Rating: ', round(confidence * 5, 2)

def remove_tags(text):
    return TAG_RE.sub(' ', text)

def getIndex(tokens, lemma):
  index = -1
  for w in tokens:
    if re.search(r'\b' + lemma + matchStemRXPattern + r'\b', w):
      index = tokens.index(w)
      break
  return index

def getShortSentence(sentence, feat):
  tokens = Tokenizer.tokenize(sentence)
  index = getIndex(tokens, feat)
  sentenceTokens = []
  for w in tokens[index: index + 16]:
    if w not in SpecialStopWords and w is not None:
      sentenceTokens.append(w)
    if w in StopWords or w in SpecialStopWords:
      break

  temp = []
  lIndex = index - 16
  if lIndex < 0:
    lIndex = 0
  tempTokens = tokens[lIndex: index]
  tempTokens.reverse()

  for w in tempTokens:
    if w not in StopWords:
      temp.append(w)
    if w in StopWords or w in SpecialStopWords:
      break

  temp.reverse()
  temp.extend(sentenceTokens)
  sentenceTokens = temp

  return ' '.join(sentenceTokens)#.replace('none', '')

def getSentence(review, featz):
    review = remove_tags(review)
    sents = nltk.sent_tokenize(review)
    if len(featz) == 1:
        for sent in sents:
            sent = sent.replace('-', ' ')
            if re.search(r'\b' + featz[0] + matchStemRXPattern + r'\b', sent):
                if len(sent) > 215:
                    sent = getShortSentence(sent, featz[0])
                return sent

if __name__ == '__main__':
    print 'I am testing sucker..'
    #review = 'Punishingly long, the movie insists that viewers spend more than two over-deliberate hours with a man whose pathology could be sketched in a few minutes. In sports, what Foxcatcher does is called "running out the clock.'
    #testSingleReview(review)
    #testTrainedClassifier(posReviews, negReviews)
    #testNewRankAlg(9, 1)
    #Testing the none thing in the review extracts

    rev = 'I would rather have a nexus 7 2014mod.'+ '<br>I own a nexus 7 second gen and it is one of the best android experience I ever had. However it had some WiFi problem since lollipop update. So I purchased nexus 9. I did expect a wonderful tablet considering its $399 price tag. But nexus 9 is not that great...'
    sent = getSentence(rev, ['android'])
    print 'the sentence is: ', sent

