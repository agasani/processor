#from textblob import TextBlob
import featureExtractor
#import featureExtractor_testOptimization
import sentencement
import sentiment
import summarizer
import mongoDataLayer
import json
from decimal import *
import time
from math import sqrt
import pymongo
from classMeronyms import Meronyms
import multiprocessing
#from multiprocessing import Pool
import cProfile
#from pathos import pp_map
#from pathos.multiprocessing import ProcessingPool as P

getcontext().prec = 3

class Main:

  def __init__(self, collection, className):
    #TODO replace phone with className later on..
    self.ClassName = className
    #self.data = dataLayer.MongoDataLayer('Phones')
    self.data = mongoDataLayer.MongoDataLayer(collection)
    #uncomment Below for feature extraction
    #self.Extractor = featureExtractor.FeatureExtractor(className.lower())
    self.Extractor = featureExtractor.FeatureExtractor(className.lower())
    mero = Meronyms(className)
    self.Meronyms = mero.GetMeronyms()
  '''def getItemFeatures(self, name, reviews):
    return self.Extractor.getItemFeatures(name, reviews)'''

  def getItemFeatures(self, name, reviews):
      return self.Extractor.getItemFeatures(name, reviews)

  def getSentiments(self, reviews, features, meronyms):
    sentiments = sentencement.getSentiments(reviews, features, meronyms)
    return sentiments

  def test(self):
    reviews = self.data.getTestReviews()
    return self.getSentiments(reviews)

  def updateDocs(self):
          docs = self.data.getAllDocs()
          for doc in docs:
                try:
                    print 'Name: ', doc['name']
                    category = 'Phones'
                    synCategories = ['Cellphones', 'Smartphones', 'Cellular', ' Smart Phones']
                    superCategory = 'Electronics'
                    reviews = []
                    if 'usersReviews' in doc:
                        reviews.extend([x['description'] for x in doc['usersReviews']])
                        #reviews.extend([x['description'] for x in doc['usersReviews'] if TextBlob(x['description']).detect_language() == 'en'])
                    if 'expertsReviews' in doc:
                        reviews.extend([x['description'] for x in doc['expertsReviews']])
                        #reviews.extend([x['description'] for x in doc['expertsReviews'] if TextBlob(x['description']).detect_language() == 'en'])

                    print 'Reviews len: ', len(reviews)
                    if len(reviews) > 10:
                        featuresAndStems = self.getItemFeatures(doc['name'], reviews)
                    else:
                        featuresAndStems = {}
                    print 'featuresAndStems: ', featuresAndStems
                    #featureWords = map(lambda x: x.keys()[0], featuresAndStems)
                    featureWords = featuresAndStems.keys()
                    print 'featuresWords to be passed to semtencemet are: ', featureWords
                    sentiments = self.getSentiments(reviews, featureWords)
                    print 'sentiments len: ', len(sentiments.keys())
                    print 'Reviews len after: ', len(reviews)
                    #print 'sentiments before adding actual and score are: ', sentiments
                    itemScore = 0.0
                    for f in sentiments.keys():
                        #print 'feature: ', f
                        #print 'actualname: ', featuresAndStems[f]
                        sentiments[f]['actualName'] = featuresAndStems[f]
                        itemScore = itemScore + sentiments[f]['score']
                    if len(sentiments.keys()) > 0:
                        itemScore = float(Decimal(itemScore)/Decimal(len(sentiments.keys())))
                        print 'itemScore: ', itemScore
                    #print 'sentiments after are: ', sentiments
                    #myClass.data.updateFeatures(doc['name'], itemScore, sentiments, len(reviews))
                    self.data.update(doc['name'], superCategory, category, synCategories, sentiments, itemScore)
                    print doc['name'], ' is successfully save..'
                except:
                    print 'Unable to update ', doc['name']

  def updateDescriptions(self):
      docs = self.data.getAllDocs()
      for doc in docs:
          description = ''
          if 'expertsReviews' in doc:
              if len(doc['expertsReviews']) > 0:
                  for d in doc['expertsReviews']:
                      if len(d['description']) > 10:
                          description = d['description'].split('Bottom Line')[0]
                          description = description.replace('\n', '')
                          description = description.replace('    ', '')
                          description = description.replace('     ', '')
                          break
          print 'Description for: ', doc['name'], ' is ', description
          self.data.updateDescription(doc['name'], description)
  
  def insert(self, name, score, syn, description, uRev, xRev, cat, superCat, feats, numRev):
      obj = {}
      obj['name'] = name
      obj['Score'] = score
      obj['Category'] = cat
      obj['SuperCategory'] = superCat
      obj['SynCategory'] = syn
      obj['description'] = description
      obj['usersReviews'] = uRev
      obj['expertsReviews'] = xRev
      obj['Features'] = feats
      obj['NumReviews'] = numRev
      print 'The obj to insert is: ', obj
      self.data.add(obj)
      
  def getReviewsFromFile(self, path):
    fileName = path
    try:
      with open(fileName, 'r') as f:
        rawReviews = f.read()
        allreviews = json.loads(rawReviews)
        reviews = []
        for rev in allreviews:
            reviews.append(rev['content'])
    except:
      print 'Could not open ', fileName
      reviews = []
    return reviews

  def getEntityRanking(self, pos, neg):
    total = pos + neg
    if total == 0:
        return 0
    z = 1.64485
    if pos > neg:
        p = float(pos)/total
        confidence = (p+z*z/(2*total)-z*sqrt((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)
    else:
        p = float(neg)/total
        confidence = -(p+z*z/(2*total)-z*sqrt((p*(1-p)+z*z/(4*total))/total))/(1+z*z/total)

    return round(confidence * 5, 2)

  def getPosandNegNum(self, reviews):
      pos = 0
      neg = 0
      for rev in reviews:
          try:
              rating = float(rev['stars'])
              if rating >= 4:
                  pos = pos + 1
              elif rating <= 2:
                  neg = neg + 1
              else:
                  content = ''
                  try:
                      content = rev['title'] + '. ' + rev['text']
                  except:
                      print 'The review has no description attr, then it should be content..'
                      content = rev['description']
                  polarity = sentiment.getReviewPolarity(content)
                  if polarity > 0:
                      pos = pos + 1
                  elif polarity < 0:
                      neg = neg + 1

          except Exception as ex:
              print 'an exception happened trying to get rating in 1st try: in getPosandNeg:  ', ex
              content = ''
              try:
                  content = rev['content']
                  print 'an exception happened trying to get rating in 2st try: in getPosandNeg: ', ex
              except:
                  content = rev
              polarity = sentiment.getReviewPolarity(content)
              if polarity > 0:
                pos = pos + 1
              elif polarity < 0:
                neg = neg + 1

      return {'pos': pos, 'neg': neg}

  def startTest(self):
      print 'Here we are..'
      pathToHariboGummyCandy = '/Users/agasani/projects/Opine/Databot/reviews.txt'

      #name = 'Haribo Gummy Candy Sugarless Gummy Bears'
      cat = 'Candy'
      syn = ['Sweets']
      superCat = 'Food'
      description = 'Sugar-free gummy bears with five real fruit flavors and jewel-like sparkling clear colors. Made with Lycasin, available in 5-pound bags only. Contains approximately 216 pieces per pound.'
      #reviews = main.getReviewsFromFile(pathToHariboGummyCandy)

      name = 'sumsang galaxy s4'
      fileName = 'Resources/sumsangG4Reviews.txt'
      reviews = self.data.getQuickTestReviews()
      numRev = len(reviews)
      uRev = reviews
      xRev = []

      print 'len reviews is: ', numRev

      #print (timeit.timeit('test()', setup="from __main__ import test"))

      #initClockTime = time.clock()
      #initWallTime = time.time()
      featuresAndStems = self.getItemFeatures(name, reviews)
      #print 'GETTING FEATURES TOOK: ', time.clock() - initClockTime, ' CLOCK TIME FOR THE PROCESS TO FINISH'
      #print 'GETTING FEATURES TOOK: ', time.time() - initWallTime, ' WALL TIME FOR THE PROCESS TO FINISH'
      featureWords = featuresAndStems.keys()
      #print 'features stems: ', featureWords
      print 'actual features: ', featuresAndStems.values()

      #initClockTime = time.clock()
      #initWallTime = time.time()
      sentiments = self.getSentiments(reviews, featureWords)
      #print 'GETTING SENTIMENTS TOOK: ', time.clock() - initClockTime, ' CLOCK TIME FOR THE PROCESS TO FINISH'
      #print 'GETTING SENTIMENTS TOOK: ', time.time() - initWallTime, ' WALL TIME FOR THE PROCESS TO FINISH'

      itemScore = 0.0
      posNneg = self.getPosandNegNum(reviews)
      itemScore = self.getEntityRanking(posNneg['pos'], posNneg['neg'])
      print 'itemScore: ', itemScore
      print 'SENTIMENTS ARE: ', sentiments
      #main.insert(name, itemScore, syn, description, uRev, xRev, cat, superCat, sentiments, numRev)

  def getFeatures(self, name, reviews):
      print 'Here we are to get features..'
      print name, ' has ', len(reviews), 'reviews.'
      featuresAndStems = self.getItemFeatures(name, reviews)
      featureStems = featuresAndStems.keys()
      print 'actual features: ', featuresAndStems.values()
      print 'MERONYMS LEN: ', len(self.Meronyms)
      sentiments = self.getSentiments(reviews, featureStems, self.Meronyms)
      #print 'Sentiments: ', sentiments
      #main.insert(name, itemScore, syn, description, uRev, xRev, cat, superCat, sentiments, numRev)
      features = []
      for stem in featureStems:
          feature = {}
          feature['stem'] = stem
          feature['name'] = featuresAndStems[stem]
          feature['pos'] = sentiments[stem]['pos']
          feature['neg'] = sentiments[stem]['neg']
          feature['neut'] = sentiments[stem]['neut']
          feature['score'] = sentiments[stem]['score']
          features.append(feature)
      print 'About to return ', len(features), ' features for ', name
      return features

  def getProductScore(self, reviews):
      posNneg = self.getPosandNegNum(reviews)
      print 'posNeg: ', posNneg
      score = self.getEntityRanking(posNneg['pos'], posNneg['neg'])
      print 'itemScore: ', score
      return score

  def start(self, products):
    print 'About to get features for ', products.count(), ' ', self.ClassName
    try:
        for prod in products:
            #add title to review for processing
            reviewsObject = prod['reviews']
            features = self.getFeatures(prod['name'], reviewsObject)
            score = self.getProductScore(reviewsObject)
            summarizr = summarizer.Summarizer()
            descriptionSummary = summarizr.start(features)
            if len(features) >= 2:
                print 'About to update Features for: ', prod['name']
                self.data.updateFeatures(prod['name'], features)
                self.data.updateScore(prod['name'], score)
                self.data.updateSummaryDescription(prod['name'], descriptionSummary)
                print 'JUST UPDATED: ', prod['name']
            else:
                print prod['name'], ' DIDNT GET ENOUGH FEATURES: ', len(features)
    except pymongo.errors.CursorNotFound as ex:
        print 'SHIT TIMEOUT: ', ex.message
        products.close()
        products = self.data.getProductWithNoFeatures()
        self.start(products)
    products.close()


if __name__ == '__main__':
    print 'WE IN MAIN..'
    #print 'TESTIN UNCOMMENT AFTER TESTING..'
    #main = Main('phones', 'phone')
    #cProfile.run('main.start()')
    #main.startTest()


    #TEST

    '''
    main = Main('phones', 'phone')
    product = main.data.getProductForTest()
    #print 'The product is: ', product
    print 'About to get features for ', product['name']
    #add title to review for processing
    reviewsObject = product['reviews'] #[rev['title'] + '. ' + rev['text'] for rev in product['reviews']]
    features = main.getFeatures(product['name'], reviewsObject)
    print 'FEATURES ARE: ', features
    score = main.getProductScore(reviewsObject)
    if len(features) >= 2:
        print 'About to update Features for: ', product['name']
        #main.data.updateFeatures(product['name'], features)
        #main.data.updateScore(product['name'], score)
        print 'JUST UPDATED: ', product['name']
    else:
        print product['name'], ' DIDNT GET ENOUGH FEATURES: ', len(features)
    '''

    #ENDTEST

    #TODO make sure the meronyms for this category are set before running it for the first time...

    collectionsNnames = [{'collection': 'phones'.strip(' '), 'name': 'phone'.strip(' ')}
                         , {'collection': 'camera'.strip(' '), 'name': 'camera'.strip(' ')}
                         , {'collection': 'tv'.strip(' '), 'name': 'tv'.strip(' ')}
                         , {'collection': 'tablet'.strip(' '), 'name': 'tablet'.strip(' ')}
                         , {'collection': 'laptop'.strip(' '), 'name': 'laptop'.strip(' ')}
                         , {'collection': 'desktop computer'.strip(' '), 'name': 'desktop'.strip(' ')}
                         , {'collection': 'ereaders'.strip(' '), 'name': 'ereader'.strip(' ')}
                         , {'collection': 'smartwatch'.strip(' '), 'name': 'smartwatch'.strip(' ')}
                         , {'collection': 'fitness tracker'.strip(' '), 'name': 'fitness tracker'.strip(' ')}
                         , {'collection': 'tv streaming media'.strip(' '), 'name': 'streamer'.strip(' ')}
                         , {'collection': 'headphone'.strip(' '), 'name': 'headphone'.strip(' ')}
                         , {'collection': 'external hard drive'.strip(' '), 'name': 'harddrive'.strip(' ')}
                         , {'collection': 'garbage-disposals'.strip(), 'name': 'garbage disposal'}
                         , {'collection':'power-tools', 'name': 'power tool'}
                         , {'collection': 'hair-loss-treatment', 'name': 'hair loss'}
                         , {'collection': 'generators', 'name': 'generator'}
                         , {'collection': 'solar-panels', 'name': 'solar panel'}
                         , {'collection': 'photography-drones', 'name': 'drone'}
                         , {'collection': 'recreational-drones', 'name': 'drone'}
                         , {'collection': 'camping-tents', 'name': 'tent'}
                         , {'collection': 'computer-monitors', 'name': 'monitor'}
                         , {'collection': 'standing-desks', 'name': 'standing-desk'}
                         , {'collection': 'computer-printers', 'name': 'printer'}
                         , {'collection': 'hoverboards', 'name': 'hoverboard'}
                         , {'collection': 'thermostats', 'name': 'thermostat'}
                         , {'collection': 'vacuum-cleaners', 'name': 'vacuum-cleaner'}

                    ]

    for coll in collectionsNnames:
        main = Main(coll['collection'], coll['name'])
        products = main.data.getProductWithNoFeatures()
        main.start(products)
        '''print 'About to get features for ', products.count(), ' products'
        try:
            for prod in products:
                #add title to review for processing
                reviewsObject = prod['reviews']
                features = main.getFeatures(prod['name'], reviewsObject)
                score = main.getProductScore(reviewsObject)
                summarizr = summarizer.Summarizer()
                descriptionSummary = summarizr.start(features)
                if len(features) >= 2:
                    print 'About to update Features for: ', prod['name']
                    main.data.updateFeatures(prod['name'], features)
                    main.data.updateScore(prod['name'], score)
                    main.data.updateSummaryDescription(prod['name'], descriptionSummary)
                    print 'JUST UPDATED: ', prod['name']
                else:
                    print prod['name'], ' DIDNT GET ENOUGH FEATURES: ', len(features)
        except pymongo.errors.CursorNotFound as ex:
            print 'SHIT TIMEOUT: ', ex.message
            products.close()'''
        products.close()


'''t = []
def test(x):
        return x * 2

print 'NUM OF PROCESSES: ', multiprocessing.cpu_count()
initClockTime = time.clock()
initWallTime = time.time()
print 'before multiprocessing..'
#for v in range(10000000):
    #t.append(test(v))

#map(test, range(1000))
#pp_map.ppmap(4, test, range(100000))

print 'After running the multi-process the result is: '#, t

print 'TIME WITHOUT MULTIPROCESSING: ', time.clock() - initClockTime, ' CLOCK TIME FOR THE PROCESS TO FINISH'
print 'TIME WITHOUT MULTIPROCESSING: ', time.time() - initWallTime, ' WALL TIME FOR THE PROCESS TO FINISH'

initClockTime = time.clock()
initWallTime = time.time()

#pool = Pool(processes=4)
#pool.map(test, range(100000))
print 'After running the multi-process the result is: '#, t

print 'TIME WITH MULTIPROCESSING: ', time.clock() - initClockTime, ' CLOCK TIME FOR THE PROCESS TO FINISH'
print 'TIME WITH MULTIPROCESSING: ', time.time() - initWallTime, ' WALL TIME FOR THE PROCESS TO FINISH'
'''

